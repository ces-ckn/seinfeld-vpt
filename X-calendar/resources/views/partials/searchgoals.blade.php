<div id="searchdiv">
	@if($key!='')
	<h2>Results for "{{$key}}"</h2>
	@else
	<h3>Please input something!!!</h3>
	@endif
	<hr>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Begin</th>
			</tr>
		</thead>
		<tbody>
			<?php $count = 0;
			if($key!='') {
			?>
			@foreach($goals as $goal)
			<tr>
				<td>{{++$count}}</td>
				<td>{{$goal->title}}</td>
				<td>{{$goal->description}}</td>
				<td>{{$goal->dateStart}}</td>
				<td>
					{!! Form::open([
							'route' => ['goal.destroy', $goal->id],
							'method'=> 'DELETE',
							'onsubmit' => 'return ConfirmDelete()'
					]) !!}
						<a class="btn btn-primary btn-sm" href="{{route('goal.view', $goal->id)}}"><span class="glyphicon glyphicon-eye-open"></span></a>
						<button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></button>
					{!! Form::close() !!}
				</td>
			</tr>
			@endforeach
			<?php } ?>
		</tbody>
	</table>
	
</div>