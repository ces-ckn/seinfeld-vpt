<div class="modal fade" id="signin-modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- @if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Whoops!</strong> There were some problems with your input.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			
			
			@endif -->

			@if(Session::has('verifyAcount'))

			<div class="alert-box success">
				<h2>{{ Session::get('verifyAcount') }}</h2>
			</div>

			@endif


			{!! Form::open([
			'route' => 'user.postlogin',
			'method' => 'POST',
			'id'	=>	'signin-form'
			]) !!}
			<div class="access-form">
				<div class="form">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<div class="header">
							<div class="modal-title">
								<h2>Sign In</h2>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="login">
							<ul>
								<li>
									<div class="testvalidation">
										<span class="un"><span class="glyphicon glyphicon-user"></span></span><input type="text" name ="email" class="text" placeholder="User Email"/>
									</div>
								</li>
								<br>
								<li>
									<div class="testvalidation">
										<span class="un"><span class="glyphicon glyphicon-lock"></span></span><input type="password" name="password" class="text" placeholder="User Password"/>
									</div>
								</li>
								<br>
								<li>
									<input type="submit" value="LOGIN" id="signin-btn" class="btn">

								</li>
								<li>
									<div class="span">
										<span class="ch"><input type="checkbox" name="remember" id="r" value="" checked > <label for="r">Remember Me</label> </span> 
										<span class="ch"><a id="forgotpassword-btn" href="#" data-toggle="modal" data-target="#forgotpassword-modal"><u><i>Forgot Password?</i></u></a></span>
									</div>
								</li>
							</ul>
							<div class="social">
								<a href="#"><div class="fb"><img id="facebook-twitter-icon" src="img\Facebook-Icon-4.png"></img> &nbsp; Login With Facebook</div></a>
								<a href="#"><div class="tw"><img id="facebook-twitter-icon" src="img\Twitter-icon.png.png"></img> &nbsp;  Login With Twitter</div></a>
							</div>
						</div><br/>
						<div class="sign">
							<div class="need">Need new account ?</div>
							<div class="up" id="signup-btn"><a href="#" data-toggle="modal" data-target="#signup-modal">Sign Up</a></div>
						</div>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
