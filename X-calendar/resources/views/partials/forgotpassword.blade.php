<div class="container">
	<div class="modal fade" id="forgotpassword-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				{!! Form::open([
				'route' => 'user.resetpassword',
				'method' => 'POST',
				'id'	=>	'forgotpassword-form'
				]) !!}
					<div class="access-form">
						<div class="form">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<div class="header">
									<div class="modal-title">
										<h2>Reset Password</h2>
									</div>
								</div>
							</div>
							<div class="modal-body">
								<div class="login">
									<ul>
										<li>
											<div class="testvalidation">
												<span class="un"><span class="glyphicon glyphicon-envelope"></span></span><input type="text" name ="email" class="text" required  placeholder="User Email"/>
											</div>
										</li>
										<br>
										<li>
											<input type="submit" value="SUBMIT" class="btn">
										</li>
									</ul>
									
								</div><br/>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
					
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>