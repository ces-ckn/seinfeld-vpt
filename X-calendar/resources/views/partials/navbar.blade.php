
<nav class="navbar" id="navbar">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="{{route('page.index')}}"><img src="\img\code-engine-studio-sm.3cf2.png" alt="Code Engine Studio"></a>
		</div>


		<?php  
				if (Session::has('email')){	
					$login='show';
					$logout='hide';
				}else{
					$login='hide';
					$logout='show';
				}
				$role = Session::get('role');
				if($role){
					$admin='show';
				}else{
					$admin='hide';
				}
		?>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav">
				<li><a href="{{route('page.index')}}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				<li><a href="#" data-toggle="modal" data-target="#about-modal">About</a></li>		
				<?php 
					$userRole = Session::get('role');
					if($userRole === "Admin"){
						echo("<li><a href='/contact'>Contact Management</a></li>");
					} else{
						echo("<li><a id='contacterror' href='#' data-toggle='modal' data-target='#contact-modal'>Contact</a></li>");
					}
				 ?>
				 <li><a <?php echo "id='".$login."'" ?> href="{{route('page.main')}}">Goals</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">

				<li><a <?php echo "id='".$logout."'" ?> href="#" class="signup-btn" data-toggle="modal" data-target="#signup-modal">Sign Up</a></li>
				<li><a <?php echo "id='".$logout."'" ?> href="#" class="signin-btn" data-toggle="modal" data-target="#signin-modal">Sign In</a></li>
				<li class="dropdown" id="user-nav">
					<a <?php echo "id='".$login."'" ?> href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
					
					<?php 
						$userAvatar = Session::get('avatar');
						if(is_null($userAvatar) || ($userAvatar==="")) {
							echo("<img src='\img\unknown-person.gif' class='img-thumbnail' width='50px' height='50px'>");
						} else{
							echo("<img src='".$userAvatar."' class='img-thumbnail' width='50px' height='50px'>");
						}
					?>

					<?php echo Session::get('name'); ?> <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li><a href=" {{ route('page.userinformation') }} ">Information</a></li>
						<li role="separator" class="divider"></li>
						<li><a <?php echo "id='".$admin."'" ?> href=" {{ route('user.management') }} ">Users Management</a></li>
						<li <?php echo "id='".$admin."'" ?> role="separator" class="divider"></li>
						<li><a href="{{route('user.logout')}}">Log out</a></li>
					</ul>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div>
</nav>
