
<div id="newgoal" class="create-form">
	<h2>Create a goal</h2>
	<hr>
	{!! Form::open([
			'route' => ['goal.store'],
			'method'=> 'POST',
			'class'	=> 'form-horizontal',
			'id'	=> 'newgoal-form'
		]) !!}
		<div class="form-group">
			<label for="title" class="control-label col-sm-3">Title</label>
			<div class="col-sm-6 testvalidate">
				<input name="title" id="titlegoal" type="text" class="form-control input-group" placeholder="Title" >
			</div>				
		</div>
		<div class="form-group">
			<label for="from" class="control-label col-sm-3">From</label>
			<div class="col-sm-3 testvalidate">
				<input name="dateStart" id="dateStart" type="date" class="form-control input-group">
			</div>	
			<div class="col-sm-3 testvalidate">
				<input name="timeStart" id="timeStart" type="time" class="form-control input-group">
			</div>			
		</div>
		<div class="form-group">
			<label for="description" class="control-label col-sm-3">Description</label>
			<div class="col-sm-6">
				<input name="description" id="description" type="text" class="form-control" placeholder="Description" >
			</div>				
		</div>
		<div class="form-group">
			<label for="repetition" class="control-label col-sm-3">Repetition</label>
			<div class="col-sm-2">
				<select name="repetition" id="repetition" class="form-control">
					<option value="Daily">Daily</option>
					<option value="Weekly">Weekly</option>
					<option value="Monthly">Monthly</option>
					<option value="Yearly">Yearly</option>
				</select>
			</div>
			<!--Extend-->
			<div class="col-sm-2">
				<select name="end" id="end" class="form-control">
					<option value="Forever">Forever</option>
					<option value="Number of iteration">Iteration</option>
					<option value="Until a date">Until</option>
				</select>	
			</div>	
			<div id="iterator"class="col-sm-2 testvalidate">
				<input id="test" name="iterator" type="number" class="form-control input-group" >
			</div>
		</div>
		<div  class="form-group">
			 <div class="col-sm-offset-3">
			 	<span class=" control-label col-sm-1">Every</span>
			 	<div id="every"class="col-sm-2 testvalidate">
					<input name="every" id="everytime" type="number" class="form-control input-group" value="1">
				</div>
				<span id="type" class=" control-label col-sm-1">day(s)</span>
			</div>
		</div>
		<div id="weekly" class="form-group">
			 <div class="col-sm-7 col-sm-offset-3">
			 	<button id="mon" type="button" class="btn btn-default btn-circle">2</button>
			 	<input id="2" name="2" type="text" class="hidden" value="0">
			 	<button id="tue" type="button" class="btn btn-default btn-circle">3</button>
			 	<input id="3" name="3" type="text" class="hidden" value="0">
			 	<button id="wed" type="button" class="btn btn-default btn-circle">4</button>
			 	<input id="4" name="4" type="text" class="hidden" value="0">
			 	<button id="thu" type="button" class="btn btn-default btn-circle">5</button>
			 	<input id="5" name="5" type="text" class="hidden" value="0">
			 	<button id="fri" type="button" class="btn btn-default btn-circle">6</button>
			 	<input id="6" name="6" type="text" class="hidden" value="0">
			 	<button id="sat" type="button" class="btn btn-default btn-circle">7</button>
			 	<input id="7" name="7" type="text" class="hidden" value="0">
			 	<button id="sun" type="button" class="btn btn-default btn-circle">CN</button>
			 	<input id="CN" name="CN" type="text" class="hidden" value="0">
			 </div>
		</div>
		<div id="endDate" class="form-group">
			<label for="to" class="control-label col-sm-3">To</label>
			<div  class="col-sm-3 testvalidate">
				<input name="dateEnd" id="dateEnd" type="date" class="form-control">
			</div>	
		</div>
		<div class="form-group">
			<label for="remin" class="control-label col-sm-3">Reminder</label>
			<div class="col-sm-3">
				<select name="reminder" id="reminder" class="form-control">
					<option value="15 minutes">15 minutes</option>
					<option value="30 minutes">30 minutes</option>
					<option value="1 hour">1 hour</option>
					<option value="3 hours">3 hours</option>
					<!-- <option value="6 hours">6 hours</option>
					 <option value="12 hours">12 hours</option>
					 <option value="24 hours">24 hours</option>
					 <option value="3 days">3 days</option>
					 <option value="a week">a week</option> --> 
				</select>
			</div>		
			<div class="checkbox col-sm-3" >
				<label><input name="autoX" id="autoX" type="checkbox" value = "1">Choose auto X</label>
			</div>		
		</div>
		<div class="form-group">
			<div class="col-sm-6 col-sm-offset-3">
				<button type="submit" class="btn btn-primary">Create</button>
			</div>
		</div>
	{!! Form::close() !!}
</div>