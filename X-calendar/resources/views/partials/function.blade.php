<?php 
function inGoal($date, $goal){
	$x = true;
	if($goal->end == 'Until a date'){
		if(strtotime($goal->dateStart) > strtotime($date) || strtotime($goal->dateEnd) < strtotime($date)) {
			$x = false;
		}
	}else if($goal->end == 'Number of iteration'){
		if(strtotime($goal->dateStart) > strtotime($date) || $goal->iterator <= 0){
			$x = false;
		}
	}else if($goal->end == 'Forever'){
		if(strtotime($goal->dateStart) > strtotime($date)){
			$x = false;
		}
	}else{
		$x = true;
	}
	return $x;
}
function dateInWeek($date){
	$dateNumOfWeek = date('w',strtotime($date));
	$x = 'sai';
	if($dateNumOfWeek == '0'){
		$x = 'CN';
	}else{
		$x = $dateNumOfWeek+'1';
	}
	return $x;
}

function eventToday($date,$goal){
	$x =false;
	/*=========ARRAY DAY IN WEEK IN GOAL==========*/
	$arr = explode(',', $goal->inWeek);
	$dayFromStart = floor((strtotime($date) - strtotime($goal->dateStart))/(24*60*60));
	$monthFromStart = (date("Y",strtotime($date))-date("Y",strtotime($goal->dateStart)))*12+(date("n",strtotime($date))-date("n",strtotime($goal->dateStart)));
	$yearFromStart = date("Y",strtotime($date))-date("Y",strtotime($goal->dateStart));
	$weekFromStart = date("W",strtotime($date))-date("W",strtotime($goal->dateStart));
	if(($goal->repetition=='Daily') && inGoal($date,$goal) && ($dayFromStart%($goal->every)== 0)){
		$x =true;
	}
	if(($goal->repetition=='Monthly')&& inGoal($date,$goal) && ($monthFromStart)%($goal->every)== 0 && (date("j",strtotime($date))==date("j",strtotime($goal->dateStart)))){
		$x =true;
	}
	if(($goal->repetition=='Yearly') && inGoal($date,$goal) && ($yearFromStart)%($goal->every)== 0 && (date("j",strtotime($date))==date("j",strtotime($goal->dateStart))) && (date("n",strtotime($date))==date("n",strtotime($goal->dateStart)))){
		$x =true;
	}
	if(($goal->repetition=='Weekly') &&  inGoal($date,$goal) && ($weekFromStart)%($goal->every)== 0 && in_array(dateInWeek($date), $arr)){
		$x =true;
	}
	return $x;
}

/*=============Check date exist in Database=======*/
	function checkDateExistInDB($date, $dates){
		$x = false;
		foreach($dates as $d){
			if($d->dateIn == $date){
				$x = true;
			}
		}
		return $x;
	}

	/*===========get result of date to mark========*/
	function getDateResult($date, $dates){
		$result = 0;
		foreach($dates as $d){
			if($d->dateIn == $date){
				$result = $d->result;
			}
		}
		return $result;
	}
?>