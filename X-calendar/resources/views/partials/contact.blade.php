<div class="container">
  <div class="modal fade" id="contact-modal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2><span class="glyphicon glyphicon-share"></span> Contact us</h2>
        </div>
        <div class="modal-body" style="padding:40px 50px;">
    
          {!! Form::open([
              'id'=>'contact-form'
          ]) !!}
            <div class="form-group testvalidate">

              <label for="email"><span class="glyphicon glyphicon-user"></span> Email</label>
              <input type="email" class="form-control" name = "email" id="email" placeholder="Enter email">
            </div>
            <div class="form-group testvalidate">
              <label for="title"><span class="glyphicon glyphicon-pencil"></span> Title</label>
              <input type="text" class="form-control" name = "title" id="title" placeholder="Title...">
            </div>
            <div class="form-group testvalidate">
              <label for="message"><span class="glyphicon glyphicon-comment"></span> Message</label>
              <textarea name="message" id="message" cols="30" rows="5" class="form-control" placeholder="Enter your message..."></textarea>
            </div>

            <button type="submit" id="contactSendBtn" class="btn btn-block btn-danger">Send</button>

          {!! Form::close() !!}
        </div>
        <div class="about-footer">
          <p>Thanks you!!!</p>
        </div>
      </div>
    </div>
  </div>
</div>
