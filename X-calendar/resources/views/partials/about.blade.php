<div class="container">
  <div class="modal fade" id="about-modal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h2 class="modal-title">VPT TEAM!!!</h2>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-3 col-sm-offset-1"><img src="/img/vien.jpg" class="img-thumbnail" alt="vien"></div>
            <div class="col-sm-7">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet nobis aperiam iste vitae facilis, laboriosam necessitatibus iure sequi numquam cumque consectetur magni, explicabo aliquam asperiores ipsam molestiae, nisi quis. Rem.</div>
          </div>
          <div class="row">
            <div class="col-sm-3 col-sm-offset-1"><img src="/img/phuoc.jpg" class="img-thumbnail" alt="phuoc"></div>
            <div class="col-sm-7 ">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet nobis aperiam iste vitae facilis, laboriosam necessitatibus iure sequi numquam cumque consectetur magni, explicabo aliquam asperiores ipsam molestiae, nisi quis. Rem.</div>
          </div>
          <div class="row">
            <div class="col-sm-3 col-sm-offset-1"><img src="/img/thuan.jpg" class="img-thumbnail" alt="thuan"></div>
            <div class="col-sm-7">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eveniet nobis aperiam iste vitae facilis, laboriosam necessitatibus iure sequi numquam cumque consectetur magni, explicabo aliquam asperiores ipsam molestiae, nisi quis. Rem.</div>
          </div>           
        </div>
        <div class="about-footer">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius accusantium reprehenderit, voluptates, totam, ullam mollitia provident molestias ab dolore nulla sequi, explicabo. Consequatur aliquam inventore eum velit dolore, porro neque.</p>
        </div>
      </div>
    </div>
  </div>
</div>