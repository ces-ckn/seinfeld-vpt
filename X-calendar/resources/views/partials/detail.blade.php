<div id="detail">
	<h2>List of goals</h2>
	<hr>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Begin</th>
			</tr>
		</thead>
		<tbody>
			<?php $count = 0; 
			?>
			@foreach($paginateGoals as $goal)
			<?php 
			if(eventToday($date,$goal)){
				echo "<tr class = viewtoday>";
			}else{
				echo "<tr>";
			} 
			?>
				<td>{{++$count}}</td>
				<td>{{$goal->title}}</td>
				<td>{{$goal->description}}</td>
				<td>{{$goal->dateStart}}</td>
				<td>
					{!! Form::open([
							'route' => ['goal.destroy', $goal->id],
							'method'=> 'DELETE',
							'onsubmit' => 'return ConfirmDelete()'
					]) !!}
						<a class="btn btn-primary btn-sm" href="{{route('goal.view', $goal->id)}}"><span class="glyphicon glyphicon-eye-open"></span></a>
						<button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></button>
					{!! Form::close() !!}
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{!! $paginateGoals->render() !!}
</div>