{!! Form::open([
	'route'		=>	'user.updateBasicInf',
	'method'	=>	'PUT',
	'id'	=>	'edit-basic-inf-div'
]) !!}
	<div class="user-content">
		<div class="testvalidation">
			<label for="name" class="col-sm-3 col-sm-offset-1">Name:</label>
			<div class="col-sm-7">
				<input type="text" class="form-control" name="name" required value="{{$user->name}}" placeholder="Enter your name!!">
			</div>
		</div>
		<br>
	</div>
	<div class="user-content">
		<div class="testvalidation">
			<label for="birthday" class="col-sm-3 col-sm-offset-1">Birthday:</label>
			<div class="col-sm-7">
				<div class="row" style="padding-bottom: 0px;">
					<div class="col-sm-12">
			            <div class="form-group">
			                <div class="input-group date" >
			                    <input class="form-control" type="date" name="birthday" id="birthday" value="{{$user->birthday}}"/>
			                    <span class="input-group-addon">
			                        <span class="glyphicon glyphicon-calendar"></span>
			                    </span>
			                </div>
			            </div>
		            </div>
				</div>
		    </div>
	    </div>
	    <br>
	</div>
	<div class="user-content">
		<label for="gender" class="col-sm-3 col-sm-offset-1">Gender:</label>
		<?php 
		if($user->gender == "Male"){
			echo '<label for="male" class="inline col-sm-2"><input type="radio" name="gender" value="Male" checked="true"> Male</label>';
			echo '<label for="female" class="inline col-sm-3"><input type="radio" name="gender" value="Female" > Female</label>';
		} else{
			echo '<label for="male" class="inline col-sm-2"><input type="radio" name="gender" value="Male"> Male</label>';
			echo '<label for="female" class="inline col-sm-3"><input type="radio" name="gender" value="Female" checked="true"> Female</label>';
		}
		
		?>
		<br>
	</div>
	<div class="user-content">
		<label for="hometown" class="col-sm-3 col-sm-offset-1">Hometown:</label>
		<div class="col-sm-7">
			<input type="text" class="form-control" name="hometown" value="{{$user->hometown}}" required placeholder="Enter your Hometown!!">
		</div>
		<br>
	</div>
	<div class="col-sm-7 col-sm-offset-4" style="margin-top:20px">
	    <div class="btn-group col-sm-10">
	        <button type="submit" class="btn btn-danger col-sm-5" >Confirm</button>
	    </div>
	</div>
{!! Form::close() !!}