{!! Form::open([
	'route'		=>	'user.updateAccountInf',
	'method'	=>	'PUT',
	'id'	=>	'edit-account-inf-div'
]) !!}
	<div class="user-content testvalidation">
		<label for="email" class="col-sm-3 col-sm-offset-1">Email:</label>
		<div class="col-sm-7">
			<input type="email" class="form-control" name="email" value="{{$user->email}}" required placeholder="Enter your Email!!">
		</div>
		<br>
	</div>
	<div class="user-content testvalidation">
		<label for="password" class="col-sm-3 col-sm-offset-1">Password:</label>
		<div class="col-sm-7">
			<input type="password" class="form-control" name="password" value="{{$user->password}}" required placeholder="Enter your Password!!">
		</div>
		<br>
	</div>
	<div class="col-sm-7 col-sm-offset-4" style="margin-top:20px">
	    <div class="btn-group col-sm-10">
	        <button type="submit" class="btn btn-danger col-sm-5 edit-account-inf-btnComfirm">Confirm</button>
	    </div>
	</div>
{!! Form::close() !!}

