<div id="account-inf-div">
	<div class="user-content">
		<label for="email" class="col-sm-3 col-sm-offset-1">Email:</label>
		<label for="email" class="col-sm-8">{{$user->email}} </label>
	</div>
	<div class="user-content">
		<label for="password" class="col-sm-3 col-sm-offset-1">Password:</label>
		<label for="password" class="col-sm-8" ><input id="user-password" type="password" value="{{$user->password}}" disabled></label>
	</div>

</div>
