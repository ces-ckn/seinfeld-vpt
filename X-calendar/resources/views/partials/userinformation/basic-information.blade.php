<div id="basic-inf-div">
	<div class="user-content">
		<label for="name" class="col-sm-3 col-sm-offset-1">Name:</label>
		<label for="name" class="col-sm-8">{{$user->name}}</label>
	</div>
	<div class="user-content">
		<label for="birthday" class="col-sm-3 col-sm-offset-1">Birthday:</label>
		<label for="birthday" class="col-sm-8">{{$user->birthday}}</label>
	</div>
	
	<div class="user-content">
		<label for="sex" class="col-sm-3 col-sm-offset-1">Gender:</label>
		<label for="sex" class="col-sm-8">{{$user->gender}}</label>
	</div>
	<div class="user-content">
		<label for="hometown" class="col-sm-3 col-sm-offset-1">Hometown:</label>
		<label for="hometown" class="col-sm-8">{{$user->hometown}}</label>
	</div>
</div>