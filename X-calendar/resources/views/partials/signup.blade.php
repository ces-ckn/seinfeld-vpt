<div class="modal fade" id="signup-modal">
	<div class="modal-dialog">
		<div class="modal-content">

			{!! Form::open([
			'id'	=>	'signup-form'
			]) !!}
			<div class="access-form">
				<div class="form">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<div class="header">
							<div class="modal-title">
								<h2>Sign Up</h2>
							</div>
						</div>
					</div>
					<div class="modal-body">
						<div class="login">
							<ul>
								<li>
									<div class="testvalidation">
										<span class="un"><span class="glyphicon glyphicon-user"></span></span><input type="text" name="name" id="username" class="text" placeholder="User Name"/>
									</div>
								</li>
								<br>
								<li>
									<div class="testvalidation">
										<span class="un"><span class="glyphicon glyphicon-envelope"></span></span><input type="text" id="emailsignup" name ="email" class="text" placeholder="User Email"/>
									</div>
								</li>
								<br>
								<li>
									<div class="testvalidation">
										<span class="un"><span class="glyphicon glyphicon-lock"></span></span><input type="password" id = "password" name="password" class="text" placeholder="User Password"/>
									</div>
								</li>
								<br>
								<li>
									<div class="testvalidation">
										<span class="un"><span class="glyphicon glyphicon-repeat"></span></span><input type="password" id = "re-password" name="password_confirmation" class="text" placeholder="Confirm Password"/>
									</div>
								</li>
								<br>
								<li>
									<input id="submitSignup" type="submit" value="SIGN UP" class="btn">
								</li>

							</ul>

						</div><br/>
					</div>
				</div>
			</div>
			{!! Form::close() !!}

		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->