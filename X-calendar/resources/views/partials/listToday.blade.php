<div id="list">
	<h2>Today</h2>
	<hr>
	<table class="table table-striped">
		<thead>
			<tr>
				<th>#</th>
				<th>Title</th>
				<th>Description</th>
				<th>Begin</th>
			</tr>
		</thead>
		<tbody>
			@include('partials.function')
			<?php 
				$count = 0; 
				$dayCurrent = date("d");
				$monthCurrent = date("m");
				$yearCurrent = date("Y");
				$date = "$yearCurrent-$monthCurrent-$dayCurrent";
			?>
			@foreach($goals as $goal)
			<?php 
			if(eventToday($date,$goal)){
			?>
			<tr>
				<td>{{++$count}}</td>
				<td>{{$goal->title}}</td>
				<td>{{$goal->description}}</td>
				<td>{{$goal->dateStart}}</td>
				<td>
					{!! Form::open([
							'route' => ['goal.destroy', $goal->id],
							'method'=> 'DELETE',
							'onsubmit' => 'return ConfirmDelete()'
					]) !!}
						<a class="btn btn-primary btn-sm" href="{{route('goal.view', $goal->id)}}"><span class="glyphicon glyphicon-eye-open"></span></a>
						<button class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></button>
					{!! Form::close() !!}
				</td>
			</tr>
			<?php } ?>
			@endforeach
		</tbody>
	</table>
</div>