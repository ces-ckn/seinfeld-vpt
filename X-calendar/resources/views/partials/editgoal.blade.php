<div class="row">
	<div id="editGoal">
		{!! Form::model($goal,[
		'route' => ['goal.update', $goal->id],
		'method'=> 'PUT',
		'class'	=> 'form-horizontal',
		'id'	=> 'editGoalForm',
		]) !!}
		<div class="form-group">
			<label for="title" class="control-label col-sm-3">Title</label>
			<div class="col-sm-9 testvalidate">
				{!! Form::text('title',null,['id' => 'title', 'class' => 'form-control', 'placeholder' => 'Title']) !!}
			</div>				
		</div>
		<div class="form-group">
			<label for="from" class="control-label col-sm-3">From</label>
			<div class="col-sm-5 testvalidate">
				<input name="dateStart" id="dateStart" type="date" class="form-control" value="{{$goal->dateStart}}">
			</div>	
			<div class="col-sm-4 testvalidate">
				<input name="timeStart" id="timeStart" type="time" class="form-control" value ="{{$goal->timeStart}}">
			</div>			
		</div>
		<div class="form-group">
			<label for="description" class="control-label col-sm-3">Description</label>
			<div class="col-sm-9">
				{!! Form::text('description',null,['id' => 'description', 'class' => 'form-control', 'placeholder' => 'description']) !!}
			</div>				
		</div>
		<div class="form-group">
			<label for="repetition" class="control-label col-sm-3">Repetition</label>
			<div class="col-sm-3">
					<!-- <select name="repetition" id="repetition" class="form-control">
						<option value="Daily">Daily</option>
						<option value="Weekly">Weekly</option>
						<option value="Monthly">Monthly</option>
						<option value="Yearly">Yearly</option>
					</select> -->
					{!! Form::select('repetition', ['Daily'=>'Daily', 'Weekly'=>'Weekly', 'Monthly'=>'Monthly', 'Yearly'=>'Yearly'],null, ['class' => 'form-control', 'id' => 'repetition']) !!}
				</div>
				<!--Extend-->
				<div class="col-sm-4">
					<!-- <select name="end" id="end" class="form-control">
						<option value="Until a date">Until</option>
						<option value="Number of iteration">Number of iteration</option>
						<option value="Forever">Forever</option>
					</select> -->
					{!! Form::select('end',['Until a date'=>'Until', 'Number of iteration'=>'Number of iteration','Forever'=>'Forever'],null,['class' => 'form-control', 'id' => 'end'])!!}	
				</div>	
				<div id="iterator"class="col-sm-2 testvalidate">
					{!! Form::input('number','iterator',null,['class' => 'form-control', 'id'=>'iter']) !!}
				</div>
			</div>
			<div  class="form-group">
				<div class="col-sm-offset-3">
					<span class=" control-label col-sm-2">Every</span>
					<div id="every"class="col-sm-3 testvalidate">
						{!! Form::input('number','every',null,['class' => 'form-control']) !!}
					</div>
					<span id="type" class=" control-label col-sm-1">day(s)</span>
				</div>
			</div>
			<?php 
				$arr = explode(',', $goal->inWeek);
			?>
			<div id="weekly" class="form-group">
				<div class="col-sm-8 col-sm-offset-3">
					@for($i=0;$i < count($arr); $i++)
					<?php 
						if($i == 6){
							$d = 'CN';
						}else{
							$d = $i+2;
						}
						if($arr[$i]!='0'){
							$red = 'dayOfweek';
						}else{
							$red = '';
						}
					?>
					<button type="button" class="btn btn-default btn-circle {{$red}}">{{$d}}</button>
					<input id="{{$d}}" name="{{$d}}" type="text" class="hidden" value="{{$arr[$i]}}">
					@endfor
				</div>
			</div>
			<div id="endDate" class="form-group ">
				<label for="to" class="control-label col-sm-3">To</label>
				<div  class="col-sm-5 testvalidate">
					<input name="dateEnd" id="dateEnd" type="date" class="form-control" value="{{$goal->dateEnd}}">
				</div>	
			</div>
			<div class="form-group">
				<label for="remin" class="control-label col-sm-3">Reminder</label>
				<div class="col-sm-5">
					{!! Form::select('reminder', [
					'15 minutes'=>'15 minutes', '30 minutes'=>'30 minutes', 
					'1 hour'=>'1 hour','1 hour'=>'1 hour',
					'3 hours'=>'3 hours','6 hours'=>'6 hours',
					'12 hours'=>'12 hours','24 hours'=>'24 hours',
					'3 days'=>'3 days','a week'=>'a week',
					],null, ['class' => 'form-control', 'id' => 'reminder']) 
					!!}
				</div>		
				<div class="checkbox col-sm-4" >
					<!-- <label><input name="autoX" id="autoX" type="checkbox" value = "1">Choose auto X</label> -->
					<label>{!! Form::checkbox('autoX', 1, null, ['id' => 'autoX']) !!}Choose auto X</label>	
				</div>		
			</div>
			<div class="form-group">
				<div class="col-sm-6 col-sm-offset-3">
					<button type="submit" class="btn btn-primary">Update</button>
				</div>
			</div>
			{!! Form::close() !!}

		</div>
		<div id="inf" goalId="{{$goal->id}}"></div>
		<div id="reportGoal">
			<div id="chartdiv"></div>
				<label for="stringofsuccess" name="stringofsuccess" class="control-label col-sm-offset-3">Success : <span id="sucsp"></span></label>	
				<label for="stringofsuccess" name="stringofsuccess" class="control-label col-sm-offset-2">Fail : <span id="failsp"></span></label>
				<!-- <label for="stringofsuccess" name="stringofsuccess" class="control-label col-sm-offset-2">Best : <span id="bestsp"></span></label>	 -->	
		</div>
	</div>