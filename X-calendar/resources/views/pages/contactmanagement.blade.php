<?php


  if (!Session::has('id')) {
    return redirect()->route('page.index');
    }
  $role = Session::get('role');
  if(!$role){
      return    Redirect::back();
  }
?>
@extends('layouts.master')
@section('head.title')
Contact Us Management
@stop
@section('body.content')
<div class="container">
  <div class="modal-header">
    <h2><span class="glyphicon glyphicon-share"></span> Contact Management</h2>
  </div>
  <div class="modal-body" style="padding:40px 50px;">
    <table>
      <tr>
        <th><input type="checkbox"></th>
        <th>Email</th>
        <th>Title</th>
        <th>Message</th>
      </tr>
        
      @foreach($contact as $element)
          <tr>
            <td><input type="checkbox"></td>
            <td>{{$element->email}}</td>
            <td>{{$element->title}}</td>
            <td>{{$element->message}}</td>
          </tr>
      @endforeach
    </table>

    <div class="form-group">
      <label for="email"><span class="glyphicon glyphicon-user"></span> Email</label>
      <input type="email" class="form-control" name = "email" id="email" placeholder="Enter email" required>
    </div>
    <div class="form-group">
      <label for="title"><span class="glyphicon glyphicon-pencil"></span> Title</label>
      <input type="text" class="form-control" name = "title" id="title" placeholder="Title..." required>
    </div>
    <div class="form-group">
      <label for="message"><span class="glyphicon glyphicon-comment"></span> Message</label>
      <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Enter your message..." required></textarea>
    </div>
    <a id="contactSendBtn" href="#" class="btn btn-block btn-danger" data-dismiss="modal">Send</a>
    
</div>
</div>
@stop