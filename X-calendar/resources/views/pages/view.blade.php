<?php
use App\User;

	if (!Session::has('id')) {
		return redirect()->route('page.index');
		}
	$id = Session::get('id');
	$user = User::where('id',$id)->get()->first();
	if(!$user->status){
		Session::flush();
		Session::flash('notifyAccount','Your account was blocked by admin');
        return redirect()->route('user.logout');
	}
?>

@extends('layouts.master')
@section('head.title')
Goal calendar
@stop
@section('head.css')
<link rel="stylesheet" href="/css/main.css">
<link rel="stylesheet" href="/css/view.css">
<script>
function goLastMonth (month, year) {
	if(month == 1){
		--year;
		month = 12;
	}
	document.location.href = "<?php $_SERVER['PHP_SELF']; ?>?month="+(month-1)+"&year="+year;
}
function goNextMonth (month,year) {
	if(month == 12){
		++year;
		month = 0;
	}
	document.location.href = "<?php $_SERVER['PHP_SELF']; ?>?month="+(month+1)+"&year="+year;
}
function goCurrentMonth (month,year) {
	if(month == 12){
		++year;
		month = 0;
	}
	document.location.href = "<?php $_SERVER['PHP_SELF']; ?>?month="+month+"&year="+year;
}
</script>
@stop
@section('body.content')
<div id="content" class="container">
	@include('partials.function')
	<?php  
	if(isset($_GET['day'])){
		$day = $_GET['day'];
	}else{
		$day = date("j",strtotime($goal->dateStart));
	}
	if(isset($_GET['month'])){
		$month = $_GET['month'];
	}else{
		$month = date("n",strtotime($goal->dateStart));
	}
	if(isset($_GET['year'])){
		$year = $_GET['year'];
	}else{
		$year = date("Y",strtotime($goal->dateStart));
	}
	$currentTime = strtotime("$year-$month-$day");//store current date
	$monthName = date("M",$currentTime);//get current month name
	$numDays = date("t",$currentTime);// get number of day in current month
	$counter = 0;

	/*===========set mark for td in tb=============*/

	function mark($time,$dates,$today_class,$i,$goal,$today)
	{
		if(checkDateExistInDB($time,$dates)){
			if(getDateResult($time,$dates)==1){
				echo "<td class='mark success ".$today_class."'>".$i."<input type='hidden' value='".$time."' goal='".$goal->id."'><p>".$today."</p></td>";
			}else if(getDateResult($time,$dates)==0){
				echo "<td class='mark fail ".$today_class."'>".$i."<input type='hidden' value='".$time."' goal='".$goal->id."'><p>".$today."</p></td>";										
			}else{
				echo "<td class='mark ready ".$today_class."'>".$i."<input type='hidden' value='".$time."' goal='".$goal->id."'><p>".$today."</p></td>";
			}
		}else{
			echo "<td class='mark ready ".$today_class."'>".$i."<input type='hidden' value='".$time."' goal='".$goal->id."'><p>".$today."</p></td>";
		}
	}
	/*============DATE CURRENT==========*/
	$dayCurrent = date("j");
	$monthCurrent = date("n");
	$yearCurrent = date("Y");
	$dateCurrent = strtotime("$yearCurrent-$monthCurrent-$dayCurrent");
	/*============For Begin button===========*/
	$monthStart = date("n",strtotime($goal->dateStart));
	$yearStart = date("Y",strtotime($goal->dateStart));

	/*=========ARRAY DAY IN WEEK IN GOAL==========*/
	$arr = explode(',', $goal->inWeek);
	?>
<div class="row">
	<div class="col-sm-10 col-sm-offset-1">
		<h2>Goal title: {{$goal->title}}</h2>
		<hr>
	</div>
</div>
<div class="row">
		<div class="col-sm-7">	
			<div>
				<div class="btn-group">
					<button type="button" class="btn btn-default" onclick="goLastMonth({{$month}},{{$year}})"><span class="glyphicon glyphicon-triangle-left"></button>
					<button type="button" class="btn btn-default" onclick="goNextMonth({{$month}},{{$year}})"><span class="glyphicon glyphicon-triangle-right"></button>
					<button type="button" class="btn btn-default" onclick="goCurrentMonth({{$monthCurrent}},{{$yearCurrent}})">Today</button>
					<button type="button" class="btn btn-default" onclick="goCurrentMonth({{$monthStart}},{{$yearStart}})">Begin</button>
				</div>
				<div class="form-group col-sm-2">
					<label for="date" class="control-label"><?php echo $monthName.", ".$year ?></label>
				</div>
			</div>
			<table class="table table-bordered col-sm-7">
				<thead>
					<tr>
						<th class="col-sm-1">Sun</th>
						<th class="col-sm-1">Mon</th>
						<th class="col-sm-1">Tue</th>
						<th class="col-sm-1">Wed</th>
						<th class="col-sm-1">Thu</th>
						<th class="col-sm-1">Fri</th>
						<th class="col-sm-1">Sat</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if($month < 10){
						$monthi = "0".$month;
					}else{
						$monthi = $month;
					}
					echo "<tr>";
					for($i = 1; $i<$numDays+1 ;$i++, $counter++){
						if($i < 10){
							$dayi = "0".$i;
						}else{
							$dayi = $i;
						}
						$time = $year."-".$monthi."-".$dayi;
						$dateWeek = dateInWeek($time);
						$timeStamp = strtotime("$year-$month-$i");
						if($i==1){
							$firstDay = date("w",$timeStamp);
							for($j=0;$j<$firstDay;$j++,$counter++){
							//black space
								echo "<td id='blank'>&nbsp</td>";
							}
						}
						if($counter%7==0){
							echo "</tr><tr>";
						}
						$dayFromStart = floor(($timeStamp - strtotime($goal->dateStart))/(24*60*60));
							if($timeStamp == $dateCurrent) {
								$today='Today';
								$today_class='today';
							}else{
								$today='';
								$today_class='';
							}
						$monthFromStart = (date("Y",$timeStamp)-date("Y",strtotime($goal->dateStart)))*12+(date("n",$timeStamp)-date("n",strtotime($goal->dateStart)));
						$yearFromStart = date("Y",$timeStamp)-date("Y",strtotime($goal->dateStart));
						$weekFromStart = date("W",$timeStamp)-date("W",strtotime($goal->dateStart));
						if($goal->repetition=='Daily'){
							if($timeStamp <= $dateCurrent && inGoal("$year-$month-$i",$goal) && ($dayFromStart%($goal->every)== 0)){
								mark($time,$dates,$today_class,$i,$goal,$today);
							}/*else if($timeStamp > $dateCurrent && inGoal("$year-$month-$i",$goal) && ($dayFromStart%($goal->every)== 0)){
								echo "<td class='mark'>".$i."</td>";
							}*/
							else{
								echo "<td class='".$today_class."'>".$i."<p>".$today."</p></td>";
							}
						}
						if($goal->repetition=='Monthly'){
							if($timeStamp <= $dateCurrent && inGoal("$year-$month-$i",$goal) && ($monthFromStart)%($goal->every)== 0 && (date("j",$timeStamp)==date("j",strtotime($goal->dateStart)))){
								mark($time,$dates,$today_class,$i,$goal,$today);
							}/*else if($timeStamp > $dateCurrent && inGoal("$year-$month-$i",$goal) && ($monthFromStart)%($goal->every)== 0 && (date("j",$timeStamp)==date("j",strtotime($goal->dateStart)))){
								echo "<td class='mark'>".$i."</td>";
							}*/
							else{
								echo "<td class='".$today_class."'>".$i."<p>".$today."</p></td>";
							}
						}
						if($goal->repetition=='Yearly'){
							if($timeStamp <= $dateCurrent && inGoal("$year-$month-$i",$goal) && ($yearFromStart)%($goal->every)== 0 && (date("j",$timeStamp)==date("j",strtotime($goal->dateStart))) && (date("n",$timeStamp)==date("n",strtotime($goal->dateStart)))){
								mark($time,$dates,$today_class,$i,$goal,$today);
							}/*else if($timeStamp > $dateCurrent && inGoal("$year-$month-$i",$goal) && ($yearFromStart)%($goal->every)== 0 && (date("j",$timeStamp)==date("j",strtotime($goal->dateStart))) && (date("n",$timeStamp)==date("n",strtotime($goal->dateStart)))){
								echo "<td class='mark'>".$i."</td>";
							}*/
							else{
								echo "<td class='".$today_class."'>".$i."<p>".$today."</p></td>";
							}
						}
						if($goal->repetition=='Weekly'){
							if($timeStamp <= $dateCurrent && inGoal("$year-$month-$i",$goal) && ($weekFromStart)%($goal->every)== 0 && in_array($dateWeek, $arr)){
								mark($time,$dates,$today_class,$i,$goal,$today);
							}/*else if($timeStamp > $dateCurrent && inGoal("$year-$month-$i",$goal) && ($weekFromStart)%($goal->every)== 0 && in_array($dateWeek, $arr)){
								echo "<td class='mark'>".$i."</td>";
							}*/
							else{
								echo "<td class='".$today_class."'>".$i."<p>".$today."</p></td>";
							}
						}
					}
					while($counter%7!=0){
						echo "<td id='blank'>&nbsp</td>";
						$counter++;
					}
					echo "</tr>";
					?>

				</tbody>
			</table>
		</div>
		<div class="col-sm-5">
			<ul class="nav nav-tabs">
				<li id="edit" class="active"><a href="#"><span class="glyphicon glyphicon-edit" ></span> Edit</a></li>
				<li id="report" ><a href="#"><span class="glyphicon glyphicon-check" ></span> Report</a></li>
			</ul>
			@include('partials.editgoal')
		</div>   
	</div>
</div>

<!-Pop up to display your pick to success or fail at the date you click->

  <!-- Modal -->
  <div class="modal fade" id="yourpick" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Are you success?</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="col-sm-5 col-sm-offset-1">
        			<a id="suc" href="#" class="btn btn-success form-control btn-block"  data-dismiss="modal"><span class="glyphicon glyphicon-ok"></span> Yes</a>
        		</div>
        		<div class="col-sm-5">
        			<a id="fail" href="#" class="btn btn-danger form-control btn-block"  data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No </a>
        		</div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop
@section('body.js')
<script src="http://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="http://www.amcharts.com/lib/3/pie.js"></script>
<script src="http://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="/js/view.js"></script>
<script src="/js/main.js"></script>
@stop