

@extends('layouts.master')
@section('head.title')
Home page
@stop
@section('head.css')
<link rel="stylesheet" href="/css/accountnotify.css">
@stop
@section('body.content')




<div id="content" class="container">
	@if(Session::has('notifyAccount'))
			
	<div id="alert-account" class="modal-dialog">
		
			<div class="header">
				<div class="modal-title">
					<h2>Notification</h2>
				</div>
			</div>
			<div >
				
						<div class="alert-box success">
							<h2>{{ Session::get('notifyAccount') }}</h2>
						</div>
						<div>
							<button id="close-account">Close</button>	
						</div>
						
				

			</div><br/>
		

	</div>

	@endif			
	
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<h1>X-Calendar</h1>
			<hr>
			<h2>The X-Calendar is a motivational tool to help you achieve your goals.</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">

			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
				</ol>
				<!– Carousel items –>
				<div class="carousel-inner" role="listbox" >
					<div class="active item">
						<img src="img/1.jpg" alt="Chania"/>
					</div>
					<div class="item">
						<img src="img/2.jpg" alt="Chania"/>
					</div>
					<div class="item">
						<img src="img/3.jpg" alt="Chania"/>
					</div>
					<div class="item">
						<img src="img/4.jpg" alt="Chania"/>
					</div>
				</div>
				<!– Carousel nav –>
				<a class="carousel-control left" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
				<a class="carousel-control right" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>		
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa voluptatum, possimus mollitia assumenda hic unde impedit praesentium officiis porro accusantium nemo qui quidem distinctio. Dolore, saepe impedit voluptate non odio.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur ex esse quas ducimus! Rem, dolorum, quos! Qui dolor cumque iure illo deleniti, quos voluptatem numquam, ad nihil nemo magni recusandae.</20></p>
			<div class="col-sm-4 col-sm-offset-4">
				@if(Session::has('id'))
				<a href="{{route('page.main')}}" class="jelly">Start your calendar</a>
				@else
				<a href="#" class="jelly" data-toggle="modal" data-target="#signin-modal">Start your calendar</a>
				@endif
				
			</div>
		</div>
	</div>
</div>

@stop
@section('body.js')
<script src="/js/index.js"></script>
@stop