<?php
  if (!Session::has('id')) {
    return redirect()->route('page.index');
    }
  $role = Session::get('role');
  if(!$role){
      return    Redirect::back();
  }
?>
@extends('layouts.master')
@section('head.title')
Users Management
@stop
@section('head.css')
<link rel="stylesheet" href="/css/jquery.dataTables.css">
<link rel="stylesheet" href="/css/user-management.css">
@stop
@section('body.content')
<div id="content" class="container">

  <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
      <h1>Users Management</h1>
      <hr>
    </div>
  </div>

  <!-- <button type="submit" class="btn btn-default btnSubmit" id="submit" name="submit" value="Search">
          <span class="glyphicon glyphicon-search"></span> Search
        </button>
   <input id="search" type="text" name = "search" placeholder="Type here">
        <input id="submit" type="submit" value="Search"> -->
        

  <form class="form-inline admincp-control-button" id="frmCheck" method="POST" action="admincp">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <button type="submit" class="btn btn-default btnSubmit" id="lockUser" name="lock" value="Lock">
          <span class="glyphicon glyphicon-lock"></span> Lock/Unlock
        </button>
        <button type="submit" class="btn btn-default btnSubmit" name="delete" value="Delete">
          <span class="glyphicon glyphicon-trash"></span> Delete
        </button>
         <button type="submit" class="btn btn-default btnSubmit" id="makeAdmin" name="admin" value="Admin">
          <span class="glyphicon glyphicon-headphones"></span> Admin
        </button> 
       
      
        <table class="table table-bordered table-hover " id="user-table">
          <thead>
            <tr>
              <th id="table-cell-center">
                <!-- <input type="checkbox" id="allcb" name="allcb"> -->
              </th>
              <th id="table-cell-center">
                No.
              </th>
              <th>
                User's email
              </th>
              <th>
                User's name
              </th>
              <th>
                Updated at
              </th>
              <th>
                Role
              </th>
              <th>
                Status
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr>
              <td id="table-cell-center">
                @if($user->role == 0)
                  <input type="checkbox" name="users[]" value="{{$user->id}}">
                @endif
              </td>
              <td id="table-cell-center" >
                {{$num++}}
              </td>
              <td>
                {{$user->email}}
              </td>
              <td>
                {{$user->name}}
              </td>
              <td>
                {{$user->updated_at}}
              </td>
              <td>
                @if($user->role)
                  Admin
                @else
                  User
                @endif
              </td>
              <td>
                @if($user->status)
                  Active
                @else
                  Locked
                @endif
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </form>

</div>
@stop
@section('body.js')
<script src="/js/jquery.dataTables.js"></script>
<script src="/js/usermanagement.js"></script>
@stop