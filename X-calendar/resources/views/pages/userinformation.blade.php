<?php
use App\User;
	if (!Session::has('id')) {
		return redirect()->route('page.index');
		}
	$id = Session::get('id');
	$user = User::where('id',$id)->get()->first();
	if(!$user->status){
		Session::flush();
		Session::flash('notifyAccount','Your account was blocked by admin');
        return redirect()->route('user.logout');
	}
?>

@extends ('layouts.master')
@section('head.title')
Information
@stop

@section('head.css')
<link rel="stylesheet" href="/css/bootstrap.css">
<link rel="stylesheet" href="/css/user-information.css">
@stop

@section('body.content')
<div class="container" id="content">
	<div class="row">
		<div class="col-sm-10 col-sm-offset-1">
			<h2>Personal Information</h2>
			<hr>
		</div>
	</div>
	<!- The left of screen ->
	<div class="col-sm-3 col-sm-offset-1">
		<div class="user-informationtitle" style="border-radius: 5px;">
			<?php 
				if(is_null($user->avatar) || ($user->avatar==="")) {
					echo("<img src='\img\unknown-person.gif' class='img-thumbnail' width='100%' height='100%'>");
				} else{
					echo("<img src='".$user->avatar."' class='img-thumbnail' width='100%' height='100%'>");
				}
			?>
		</div>
		<div class="user-informationtitle" style="border-radius: 5px;">
			<a href="#" id="upload-btn"><span class="glyphicon glyphicon-upload"></span> Upload a Photo</a>
			@include('partials/userinformation/upload')
		</div>

		<div class="user-informationtitle">		
			<label for="information">Personal Information</label>
		</div>
		<div class="user-information">
			<label for="name"><span class="glyphicon glyphicon-user"></span> Name: {{$user->name}}</label>
			<label for="birthday"><span class="glyphicon glyphicon-bell"></span> Birthday: {{$user->birthday}}</label>
		</div>
	</div>

	<!- The middle of screen ->
	<div class="col-sm-6" style="margin-left:20px;">

		<!- Navbar-User Information ->
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs">
					<li id="basic-inf-btn"><a href="#"><span class="glyphicon glyphicon-user" ></span> Basic Information</a></li>
					<li id="account-inf-btn"><a href="#"><span class="glyphicon glyphicon-eye-open" ></span> Account Information</a></li>
				</ul>
			</div>   
		</div>

		<!- User-Information ->
		<div class="user-informationtitle" style="font-size: 17px;">
			<label for="basicinformation"><b>User Information</b></label>
			<a href="#" id="edit-basic-inf-btn"><span class="glyphicon glyphicon-pencil" style="float:right;"></span></a>
			<a href="#" id="edit-account-inf-btn"><span class="glyphicon glyphicon-pencil" style="float:right;"></span></a>	
		</div>
		<div class="user-information">
			@include('partials/userinformation/basic-information')
			@include('partials/userinformation/account-information')
			@include('partials/userinformation/edit-basic-information')
			@include('partials/userinformation/edit-account-information')
			
		</div>	
	</div>
</div>
@stop

@section('body.js')
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/userinformation.js"></script>
@stop
