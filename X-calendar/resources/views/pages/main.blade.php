<?php
use App\User;

	if (!Session::has('id')) {
		return redirect()->route('page.index');
		}
	$id = Session::get('id');
	$user = User::where('id',$id)->get()->first();
	if(!$user->status){
		Session::flush();
		Session::flash('notifyAccount','Your account was blocked by admin');
        return redirect()->route('user.logout');
	}
?>
@extends('layouts.master')
@section('head.title')
Start your calendar
@stop
@section('head.css')
	<link rel="stylesheet" href="/css/main.css">
@stop

@section('body.content')
@include('partials.function')
<?php 
	$dayCurrent = date("d");
	$monthCurrent = date("m");
	$yearCurrent = date("Y");
	$date = "$yearCurrent-$monthCurrent-$dayCurrent";
	$goals = session::get('goals');
	$count = 0;

	foreach ($goals as $goal ) {
		if(eventToday($date,$goal)){
			$count++;
		}
	}

?>
<div id="content" class="container">
	<div class="row">
		
		<!-MENU AT THE LEFT->
		<div id="left" class="col-sm-2">
			<ul class="vertical_menu">
				<li class="active" id="flip2"><a  href="#">Detail</a></li>
				<li id="flip1"><a  href="#">New Goal</a></li>						
			</ul>

			<div id="clock" class="light">
				<div class="display">
					<div class="weekdays"></div>
					<div class="ampm"></div>
					<div class="alarm"></div>
					<div class="digits"></div>
				</div>
			</div>

			<div id="calendar" cookie="{{session::get('id')}}">
					
					<p id="Today">TODAY</p>
					<p id="calendar-day"></p>
					<p id="calendar-date"></p>
					<p id="numEvents">{{$count." goals"}}</p>
					<p id="calendar-month-year"></p>
			</div>
		</div>

		<!-CONTENT AT THE RIGHT->
		<div id="right" class="col-sm-10">
			<form id="searchbox" action="">
				<input id="search" name="keysearch" type="text" placeholder="Type here">
				<button id="submit" type="submit" value="Search"><span class="glyphicon glyphicon-search"></button>
			</form>
			<div id="searchresult"></div>

			<!-main page load first->


			<!-Form creat when click new goal->

			@include('partials.newgoal')

			<!-list of goals appear when click to detail->
			
			@include('partials.detail')
			
			<div id="listGoalsToday"></div>
		</div>
	</div>
</div>
@stop
@section('body.js')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="/js/main.js"></script>
<script src="/js/vertical-menu.js"></script>
<script src="/js/script.js"></script>
@stop