
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('head.title')</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
	<link rel="stylesheet" href="http://getbootstrap.com/dist/css/bootstrap.css">
	<link rel="stylesheet" href="/css/app.css">
	<link rel="stylesheet" href="/css/signup-signin.css">
	<link rel="stylesheet" href="/css/about.css">
	<link rel="stylesheet" href="/css/style.css">
	<link rel="stylesheet" href="/css/navigation.css">

	<link rel="stylesheet" href="http://bootstrapheroes.com/semantriademo/downloader/light-blue/ui_icons.html">
	<link rel="stylesheet" href='http://fonts.googleapis.com/css?family=Comfortaa:400,700'  type='text/css'>
	<link rel="stylesheet" href="https://jqwidgets.com/public/jqwidgets/styles/jqx.base.css">
	<link rel="stylesheet" href="https://jqwidgets.com/public/jqwidgets/styles/jqx.energyblue.css">

	<link href='http://fonts.googleapis.com/css?family=Comfortaa:400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="/css/messi.min.css" />
	<link rel="stylesheet" href="/css/messi.css" />
	@yield('head.css')
</head>
<body>

	@include('partials.navbar')		
	@include('partials.about')
	@include('partials.contact')
	@include('partials.signin')
	<div class="modal fade" id="signup-modal">
		<div class="modal-dialog">
			<div class="modal-content">

				{!! Form::open([
				'id'	=>	'signup-form'
				]) !!}
					<div class="access-form">
						<div class="form">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<div class="header">
									<div class="modal-title">
										<h2>Sign Up</h2>
									</div>
								</div>
							</div>
							<div class="modal-body">
								<div class="login">
									<ul>
										<li>
											<div class="testvalidation">
												<span class="un"><span class="glyphicon glyphicon-user"></span></span><input type="text" name="name" id="username" class="text" placeholder="User Name"/>
											</div>
										</li>
										<br>
										<li>
											<div class="testvalidation">
												<span class="un"><span class="glyphicon glyphicon-envelope"></span></span><input type="text" id="emailsignup" name ="email" class="text" placeholder="User Email"/>
											</div>
										</li>
										<br>
										<li>
											<div class="testvalidation">
												<span class="un"><span class="glyphicon glyphicon-lock"></span></span><input type="password" id = "password" name="password" class="text" placeholder="User Password"/>
											</div>
										</li>
										<br>
										<li>
											<div class="testvalidation">
												<span class="un"><span class="glyphicon glyphicon-repeat"></span></span><input type="password" id = "re-password" name="password_confirmation" class="text" placeholder="Confirm Password"/>
											</div>
										</li>
										<br>
										<li>
											<input id="submitSignup" type="submit" value="SIGN UP" class="btn">
										</li>

									</ul>
									
								</div><br/>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
					
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	@include('partials.forgotpassword')
	@yield('body.content')
	@include('partials.footer')

	<script src="/js/jquery.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>

	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script src="/js/validates/contactvalidate.js"></script>
	<script src="/js/validates/newgoalvalidate.js"></script>
	<script src="/js/validates/editgoalvalidate.js"></script>
	@yield('body.js')
	<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
	<script src="/js/validates/signin-signup-rspassword-validate.js"></script>
	<script src="/js/validates/userinformation-validate.js"></script>
	<script src="/js/signin-signup-rspassword.js"></script>
	<script src="/js/contact.js"></script>
	<script src="/js/messi.min.js"></script>
	<script src="/js/messi.js"></script>
</body>
</html>