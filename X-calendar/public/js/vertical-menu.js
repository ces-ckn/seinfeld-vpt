$(document).ready(function(){
    $("#flip1").click(function(){
        $("#flip1").addClass('active');
        $("#flip2").removeClass('active');
        $("#newgoal").slideDown("slow");
        $("#detail").slideUp("slow");
        $("#list").slideUp('slow');
        $("#searchdiv").slideUp("slow");
        $(".testvalidate").removeClass('has-error');
        $(".testvalidate span").hide();
        $('#titlegoal').val('');
        $('#dateStart').val(null);
        $('#dateEnd').val(null);
        $('#timeStart').val(null);
        $('#test').val(null);
        $('#everytime').val('1');
    });
    $("#flip2").click(function(){
        $("#flip2").addClass('active');
        $("#flip1").removeClass('active');
    	$("#newgoal").slideUp("slow");
        $("#detail").slideDown("slow");
        $("#list").slideUp('slow');
        $("#searchdiv").slideUp("slow");
    });
});

$(window).load(function(){
    var valcookie = "id"+$('#calendar').attr('cookie');
    var date = new Date();
     if (document.cookie.indexOf (valcookie) == -1)
    {
        var d = new Date();
        d.setTime(d.getTime() + 16*60*60*1000);
        var expires = "expires=" + d.toGMTString();
        document.cookie = "user"+valcookie+" = "+valcookie+";"+expires;
        var count = $('#numEvents').text();
        new Messi('You have '+ count +' goals today!!!', {title: 'Hello!!!',titleClass: 'info', height:50, modal: true });
    }
});