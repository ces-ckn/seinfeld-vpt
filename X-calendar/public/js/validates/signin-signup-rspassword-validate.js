//============== sign in validate ==================
$.validator.addMethod("EMAIL", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{1,5}$/i.test(value);
}, "Please enter a valid email address.");

$('#signin-form').validate({
    rules: {
        email: {
        	required: true,
        	email: true 
        },
        password: {
        	required: true,
            minlength: 6,
            maxlength: 32
            
        }
    },
    highlight: function(element) {
        $(element).closest('.testvalidation').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.testvalidation').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'inline',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

//==================== sign up =====================
 $.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
 },"Name must contain only characters");

$('#signup-form').validate({
    rules: {
    	name: {
    		required: true,
    		number: false,
    		maxlength: 25,
            alpha: /^[a-zA-Z\s]+$/
    	},
        email: {
        	required: true,
        	email: true 
        },
        password: {
        	required: true,
            minlength: 6,
            maxlength: 32
        },
        password_confirmation: {
        	required: true,
            minlength: 6,
            maxlength: 32,
            equalTo: "#password"

        }
    },
    highlight: function(element) {
        $(element).closest('.testvalidation').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.testvalidation').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'inline',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    },
    submitHandler: function (form) { // for demo
        return false; // for demo
    }
});

//================= Reset Password=================
$('#forgotpassword-form').validate({
    rules: {
        email: {
            required: true,
            email: true,
            EMAIL: true   
        },
    },
    highlight: function(element) {
        $(element).closest('.testvalidation').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.testvalidation').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'inline',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});