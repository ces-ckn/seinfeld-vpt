function yesterday()
{
 var d = new Date();
 var day = d.getDate() -1;
 var month = d.getMonth() + 1;
 var year = d.getFullYear();
 return year + "-" + month + "-" + day;
}
jQuery.validator.addMethod("soDuong", function(value, element) {
 if(value <= 0){
    return false;
}else{
    return true;
}
}, "This value must be greater than 0"
);
jQuery.validator.addMethod("afterYesterday", function(value, element) {
 if(new Date(yesterday()) > new Date(value)){
    return false;
}else{
    return true;
}
}, "Date start must be after yesterday"
);
jQuery.validator.addMethod("afterdate", function(value, element, param) {
 return new Date(value) > new Date($(param).val());
}, "Date end after date start"
);
jQuery.validator.addMethod("validDate", function(value, element) {
    var d = new Date(value);
    if(d.getTime() < 0){
        return false;
    }else{
        return true;
    }
}, "The date is not a valid date."
);
$('#newgoal-form').validate({
    rules: {
        title: {
            minlength: 1,
            required: true
        },
        dateStart: {
            required: true,
            validDate: true,
            afterYesterday: true
        },
        timeStart: {
            required: true
        },
        iterator: {
           soDuong: true
       },
       every: {
           soDuong: true
       },
       dateEnd:{
           validDate: true,
           afterdate: '#dateStart'
       }
   },
   messages: { 
    title: { 
        required: "This field is required.", 
        minlength: "Please enter at least 1 characters."
    }, 
    dateStart: {
        required: "This field is required.",
        date: "The date is not a valid date."
    },
    timeStart: {
        required: "This field is required."
    }     
},
highlight: function(element) {
    $(element).closest('.testvalidate').addClass('has-error');
},
unhighlight: function(element) {
    $(element).closest('.testvalidate').removeClass('has-error');
},
errorElement: 'span',
errorClass: 'help-block',
errorPlacement: function(error, element) {
    if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
    } else {
        error.insertAfter(element);
    }
}
});