//============== account information validate ==================
$.validator.addMethod("EMAIL", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
}, "Please enter a valid email address.");

$('#edit-account-inf-div').validate({
    rules: {
        email: {
        	required: true,
        	email: true,
            EMAIL: true   
        },
        password: {
        	required: true,
            minlength: 6,
            maxlength: 32
            
        }
    },
    highlight: function(element) {
        $(element).closest('.testvalidation').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.testvalidation').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'inline',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});

//==================== basic information validate =====================
 $.validator.addMethod("alpha", function(value, element) {
    return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
 },"Name must contain only characters.");

$.validator.addMethod("acceptBirthday", function(value, element) {
    var currentDate = new Date();
    return Date.parse(value) < currentDate;
}, "Birthday must be before or today.");

$('#edit-basic-inf-div').validate({
    rules: {
    	name: {
    		required: true,
    		number: false,
    		maxlength: 64,
            alpha: /^[a-zA-Z\s]+$/
    	},
        birthday: {
            date: true,
            acceptBirthday: true
        },
        hometown: {
            required: false
        }

    },
    highlight: function(element) {
        $(element).closest('.testvalidation').addClass('has-error');
    },
    unhighlight: function(element) {
        $(element).closest('.testvalidation').removeClass('has-error');
    },
    errorElement: 'span',
    errorClass: 'inline',
    errorPlacement: function(error, element) {
        if(element.parent('.input-group').length) {
            error.insertAfter(element.parent());
        } else {
            error.insertAfter(element);
        }
    }
});