$('#contact-form').validate({
    rules: {
        email: {
            required: true,
            email:true
        },
        title: {
            required: true,
            minlength: 6
        },
        message: {
            required: true,
            minlength: 10
        }
   },
highlight: function(element) {
    $(element).closest('.testvalidate').addClass('has-error');
},
unhighlight: function(element) {
    $(element).closest('.testvalidate').removeClass('has-error');
},
errorElement: 'span',
errorClass: 'help-block',
errorPlacement: function(error, element) {
    if(element.parent('.input-group').length) {
        error.insertAfter(element.parent());
    } else {
        error.insertAfter(element);
    }
},
submitHandler: function (form) { // for demo
        $('#email').val('');
        $('#title').val('');
        $('#message').val('');
        new Messi('Thank you for your feedback!!!', {title: 'Hello!!!',titleClass: 'info', height:'auto', width: 'auto',center: false, viewport: {top: '200px', left:'30%'}});
        return false; // for demo
    }
});