$(document).ready(function() {
	$('td.mark').bind('click',function(){
		if($(this).is('.ready')){
			$(this).removeAttr('class').addClass('success');
			var date = $(this).children('input').attr('value');
			var goal_id = $(this).children('input').attr('goal');
			var result = 1;
			$.get('/mark',{date:date, goal_id:goal_id, result:result},function(data){
				/*alert(data);*/
			});
		}else if($(this).is('.success')){
			$(this).removeAttr('class').addClass('fail');
			var date = $(this).children('input').attr('value');
			var goal_id = $(this).children('input').attr('goal');
			var result = 0;
			$.get('/mark',{date:date, goal_id:goal_id, result:result},function(data){
				/*alert(data);*/
			});
		}else{
			$(this).removeAttr('class').addClass('mark ready');
			var date = $(this).children('input').attr('value');
			var goal_id = $(this).children('input').attr('goal');
			$.get('/delDate',{date:date, goal_id:goal_id},function(data){
				/*alert(data);*/
			});
		}
	});
	$("#edit").click(function(){
		$(this).addClass('active');
		$("#report").removeClass('active');
		$("#editGoal").slideDown("slow");
		$("#reportGoal").slideUp("slow");
	});
	var cw = $('td').width()*120/100;
	$('td').css({'height':cw+'px '});
	$("#report").click(function(){
		$(this).addClass('active');
		$("#edit").removeClass('active');
		var goalId = $('#inf').attr('goalId');
		$.get('/report',{goalId:goalId},function(data){
			document.getElementById("inf").innerHTML=data;
			var suc = $('#data-report').attr('suc');
		  	var fail = $('#data-report').attr('fail');
		  	var best = $('#data-report').attr('best');
		  	var chart = AmCharts.makeChart( "chartdiv", {
			  "type": "pie",
			  "theme": "light",
			  "dataProvider": [{
			  "result": "Success",
			  "litres": suc
			  },{
			  "result": "Fail",
			  "litres": fail
			  }],
			  "valueField": "litres",
			  "titleField": "result",
			  "balloon":{
			  "fixedPosition":true
				},
			  "export": {
			  "enabled": true
			 	}
			});
			$('#sucsp').text(suc);
			$('#failsp').text(fail);
			$('#bestsp').text(best);
		});
		$("#editGoal").slideUp("slow");
		$("#reportGoal").slideDown("slow");
		
	});
});


