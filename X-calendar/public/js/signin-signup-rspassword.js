$(document).ready(function(){
	$('.signup-btn').on('click', function(){
		$('#signup-form').find('.text').val("");
		$('#signup-form').find('.testvalidation').removeClass('has-error');
		$('.testvalidation').find('.inline').hide();
	});

	$('#signup-btn').on('click', function(){
		$("#signin-modal").modal('hide');
		$('#signup-form').find('.text').val("");
		$('#signup-form').find('.testvalidation').removeClass('has-error');
		$('.testvalidation').find('.inline').hide();
	});

	$('.signin-btn').on('click', function(){
		if( $('.testvalidation').hasClass('has-error')){
			$('.has-error').find('.text').val("");
			$('#signin-form').find('.testvalidation').removeClass('has-error');
			$('.testvalidation').find('.inline').hide();
		}
	});

	$('#forgotpassword-btn').on('click', function(){
		if( $('.testvalidation').hasClass('has-error')){
			$('.has-error').find('.text').val("");
			$('#forgotpassword-form').find('.testvalidation').removeClass('has-error');
			$('.testvalidation').find('.inline').hide();
		}
	});

	$('#submitSignup').click(function(){
		var username = $('#username').val();
		var email = $('#emailsignup').val();
		var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{1,4})+$/; 
		var password = $('#password').val();
		var repassword = $('#re-password').val();
		if(username!=''&&username.length<26&&username==username.match(/^[a-zA-Z\s]+$/)&&email!=''&&filter.test(email)&&password!=''&&password.length>5&&password.length<33&&password==repassword){
			$("#signup-modal").modal('hide');
			new Messi('Thanks for signing up! Please check your email to confirm your account!!!', {title: 'Alert!',titleClass: 'info', height:100, width: '250px'});
			$.get('/signup',{username:username,email:email,password:password},function(data){
			});
		}
	});

});