$(document).ready(function(){
	/*Set up User Information for the first time*/
	$('#basic-inf-btn').find('a').css({'background-color':'#f15927', 'color':'#fff'});
	$('#account-inf-btn').find('a').css({'background-color':'lightgray', 'color':'#000'});
	$('#edit-account-inf-btn').hide();

	/*Set up upload avatar button*/
	$('#upload-btn').on('click', function(event){
		event.preventDefault();
		$('#upload-div').toggle('slow');
	});

	/*Basic Information*/
	$('#basic-inf-btn').on('click',function(event){
		event.preventDefault();
		$(this).find('a').css({'background-color':'#f15927', 'color':'#fff'});
		$('#account-inf-btn').find('a').css({'background-color':'lightgray', 'color':'#000'});
		$('#edit-basic-inf-btn').show();
		$('#edit-account-inf-btn').hide();

		$('#account-inf-div').hide();
		$('#edit-account-inf-div').hide();
		$('#edit-basic-inf-div').hide();
		$('#basic-inf-div').toggle('slow');

	});

	$('#edit-basic-inf-btn').on('click',function(event){
		event.preventDefault();
		$('#basic-inf-div').hide();
		$('#account-inf-div').hide();
		$('#edit-account-inf-div').hide();
		$('#edit-basic-inf-div').toggle('slow');

	});	

	/*Account Information*/
	$('#account-inf-btn').on('click',function(event){
		event.preventDefault();
		$(this).find('a').css({'background-color':'#f15927', 'color':'#fff'});
		$('#basic-inf-btn').find('a').css({'background-color':'lightgray', 'color':'#000'});
		$('#edit-account-inf-btn').show();
		$('#edit-basic-inf-btn').hide();

		$('#basic-inf-div').hide();
		$('#edit-basic-inf-div').hide();
		$('#edit-account-inf-div').hide();
		$('#account-inf-div').toggle('slow');

	});

	$('#edit-account-inf-btn').on('click',function(event){
		event.preventDefault();
		$('#basic-inf-div').hide();
		$('#edit-basic-inf-div').hide();
		$('#account-inf-div').hide();
		$('#edit-account-inf-div').toggle('slow');

		/*$.ajax({
			url: '/editAccountInf',
			type: 'GET',
			dataType: 'text',
			success: function(response){
				$('#user-inf').html(response);
			}
		});*/
	});

});
