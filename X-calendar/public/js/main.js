$(document).ready(function(){
    $("#end").change(function(){
        var select = $("#end").find("option:selected").val();
        if(select == "Number of iteration"){
             $('#iterator').slideDown("slow");
             $('#endDate').slideUp("slow");
        }else if(select == "Until a date"){
            $('#endDate').slideDown("slow");
            $('#iterator').slideUp("slow");
        }else{
             $('#endDate').slideUp("slow");
            $('#iterator').slideUp("slow");
        }
    });

    $("#repetition").change(function(){
        var select = $("#repetition").find("option:selected").val();
        if(select == "Weekly"){
            $("#type").text("week(s)");
            $('#weekly').slideDown("slow");
        }else if(select == "Monthly"){
            $("#type").text("month(s)");
            $('#weekly').slideUp("slow");
        }else if(select == "Yearly"){
            $("#type").text("year(s)");
            $('#weekly').slideUp("slow");
        }else{
            $("#type").text("day(s)");
            $('#weekly').slideUp("slow");
        }
    });
    $('.btn-circle').click(function(){
        $(this).toggleClass('dayOfweek');
        var value = '#'+$(this).text();
        if ($(this).is('.dayOfweek')) {
        $(value).attr('value',$(this).text());
        }else{
            $(value).attr('value','0');
        }
    });

    var select1 = $("#repetition").find("option:selected").val();
        if(select1 == "Weekly"){
            $("#type").text("week(s)");
            $('#weekly').slideDown("slow");
        }else if(select1 == "Monthly"){
            $("#type").text("month(s)");
            $('#weekly').slideUp("slow");
        }else if(select1 == "Yearly"){
            $("#type").text("year(s)");
            $('#weekly').slideUp("slow");
        }else{
            $("#type").text("day(s)");
            $('#weekly').slideUp("slow");
        }
    var select = $("#end").find("option:selected").val();
        if(select == "Number of iteration"){
             $('#iterator').show();
             $('#endDate').hide();
        }else if(select == "Until a date"){
            $('#endDate').show();
            $('#iterator').hide();
        }else{
            $('#iterator').hide();
            $('#endDate').hide();
        }
    $('#calendar').click(function(event){
        event.preventDefault();
        $('#detail').slideUp("slow");
        $("#newgoal").slideUp("slow");
        $("#searchdiv").slideUp("slow");
        $("#flip1").removeClass('active');
        $("#flip2").removeClass('active');
        $.get('/list',function(data){
            document.getElementById("listGoalsToday").innerHTML=data;
        });
    });
    // ========search using ajax=============
    $('#submit').click(function(event){
        event.preventDefault();
        $("#flip1").removeClass('active');
        $("#flip2").removeClass('active');
        $('#detail').slideUp("slow");
        $("#newgoal").slideUp("slow");
        $("#list").slideUp('slow');
        var key = $('#search').val();
        $.get('/search',{key:key},function(data){
            document.getElementById("searchresult").innerHTML=data;
        });
        $('#search').val('');
    });
});

function ConfirmDelete()
  {
  var x = confirm("Are you sure you want to delete?");
  if (x)
    return true;
  else
    return false;
  }
