<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Goal extends Model
{
    protected $table = 'goals';
    protected $fillable = [
    	'title',
        'user_id',
    	'description',
    	'dateStart',
    	'timeStart',
    	'repetition',
        'end',
    	'iterator',
    	'autoX',
    	'reminder',
        'dateEnd',
        'inWeek',
        'every',
        'best',
        'countBest'
    ];
}
