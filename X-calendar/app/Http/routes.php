<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Mail as Mail;

Route::get('/', [
	'as'	=> 'page.index',
	'uses'	=> 'PagesController@index'
]);

get('/contact',[
	'as'	=> 'page.contact',
	'uses'	=> 'PagesController@contact'
]);

//==================login and register(start)================
/*get('/register',[
	'as'	=>	'user.register',
	'uses'	=>	'UsersController@register'
]);*/
get('/signup','UsersController@storeajax');

/*post('/store',[
	'as'	=>	'user.store',
	'uses'	=>	'UsersController@store'
]);*/

Route::get('register/verify/{confirmationCode}', [
    'as' => 'user.confirmation',
    'uses' => 'UsersController@confirm'
]);

post('/login',[
	'as'	=>	'user.postlogin',
	'uses'	=>	'UsersController@postLogin'
]);

get('/forgotpassword',[
	'as'    =>  'user.forgotpassword',
	'uses'  =>  'UsersController@forgotPassword'
	]);

post('/resetpassword',[
	'as'    =>  'user.resetpassword',
	'uses'  =>  'UsersController@resetPassword'
	]);
get('/logout',[
	'as'	=>	'user.logout',
	'uses'	=>	'UsersController@logout'
	]);

//====================login and register(end)==================

//=====================contact us==========================


get('/main',[
	'as'	=>	'page.main',
	'uses'	=>	'PagesController@main'
]);


get('/contactus','ContactsController@postContact');

//=======================users management===================

get('/usermanagement',[
	'as'	=>	'user.management',
	'uses'	=>	'UsersController@management'
]);

get('/searchuser','UsersController@searchUsers');

post('/admincp',[
	'as'	=>	'user.buttonadmin',
	'uses'	=>	'UsersController@buttonAdmin'
]);
//================banned account==============================
Route::filter('banned',function(){
		if(Auth::check() && !Auth::user()->status){
			return redirect('/logout');
		}
});
Route::group(array('before' => 'banned'), function()
{       
	//use your routes 
	

});
//=============================================

//====================================================

get('/userinformation',[
	'as'	=>	'page.userinformation',
	'uses'	=>	'PagesController@userInformation'
]);

get('/view',[
	'as'	=>	'page.view',
	'uses'	=>	'PagesController@view'
]);
post('/view',[
	'as'	=>	'page.view',
	'uses'	=>	'PagesController@view'
]);

//=================GOALS=====================
post('/view',[
	'as'	=>  'goal.store',
	'uses'	=>	'GoalsController@store'
]);

get('/main/{id}/view',[
	'as'	=>	'goal.view',
	'uses'	=>	'GoalsController@view'
]);

get('/main/{id}/edit',[
	'as'	=>	'goal.edit',
	'uses'	=>	'GoalsController@edit'
]);

put('/main/{id}',[
	'as'	=> 'goal.update',
	'uses'	=> 'GoalsController@update'
]);
delete('/main/{id}',[
	'as'	=> 'goal.destroy',
	'uses'	=> 'GoalsController@destroy'
]);
get('/mark','GoalsController@addDate');
get('/delDate','GoalsController@delDate');
get('/list','GoalsController@listgoals');
get('/search','GoalsController@searchgoals');
get('/report','GoalsController@report');
//=========================================

get('/result',[
	'as'	=>	'example.result',
	'uses'	=>	'PagesController@result'
	]);

//=============User Information Route================
get('/basicInf','UserInformationController@basicInf');
get('/accountInf','UserInformationController@accountInf');
get('/editBasicInf','UserInformationController@editBasicInf');
get('/editAccountInf','UserInformationController@editAccountInf');

//=================Upload picture=====================
Route::get('upload', function() {
  return View::make('partials/userinformation/upload');
});
Route::post('/upload', 'UserInformationController@upload');

//=============Update User-Information================
put('/userinformation/updateBasicInf',[
	'as'	=>	'user.updateBasicInf',
	'uses'	=>	'UserInformationController@updateBasicInf'
]);

put('/userinformation/updateAccountInf',[
	'as'	=>	'user.updateAccountInf',
	'uses'	=>	'UserInformationController@updateAccountInf'
]);

