<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GoalFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|min:1',
            'dateStart'     => 'required|date|after:yesterday',
            'timeStart'     => 'required',
            'repetition'    => 'required',
            'iterator'      => 'integer',
            'dateEnd'       => 'date|after:dateStart',
            'every'         => 'integer'
        ];
    }
}
