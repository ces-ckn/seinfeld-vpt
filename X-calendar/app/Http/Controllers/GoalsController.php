<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Cookie;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Goal;
use App\Date;
use App\User;
use DB;
use App\Http\Requests\GoalFormRequest;
use App\Http\Requests\EditGoalFormRequest;

class GoalsController extends Controller
{
    public function view($id)
    {
        $goal = Goal::find($id);
        $dates = Date::where('goal_id',$id)->get();     
        return view('pages.view')->with(['goal'=>$goal,'dates'=>$dates]);
    }
    public function edit($id)
    {
        $goal = Goal::find($id);
        return view('pages.editGoals',compact('goal'));
    }
    public function report()
    {
        $id = Input::get('goalId');
        $goal = Goal::find($id);
        $succ = Date::where('goal_id',$id)->where('result','1')->count();
        $total = Date::where('goal_id',$id)->count();
        $fail = $total-$succ;
        $best = $goal->best;
        return view('partials.inf-report')->with(['a'=>$succ, 'b' => $fail, 'best'=>$best]);
    }
    public function addDate()
    {
        $goal_id = Input::get('goal_id');
        $dateIn = Input::get('date');
        $result = Input::get('result');
        $goal = Goal::find($goal_id);
       
        $ctbest = 0;
        
        $rs = Date::where('goal_id',$goal_id)->where('dateIn',$dateIn)->get()->first();
        if($rs != null){
            $rs->update([
            'goal_id' =>  $goal_id,
            'dateIn'  =>  $dateIn,
            'result'  =>  $result,
            ]);
            return 'update';
        }else{
            if($goal->end == "Number of iteration"){
                $itr = $goal->iterator-1;
                $goal->update([
                    'iterator'=> $itr
                ]);
            }
            Date::create([
            'goal_id' =>  $goal_id,
            'dateIn'  =>  $dateIn,
            'result'  =>  $result,
            ]);
            if($result == 0){
                $goal->update([
                    'countBest'=> $ctbest
                    ]); 
            }else{
                $ctbest = $goal->countBest+1;
                if($ctbest > $goal->best){
                    $best = $ctbest;
                    $goal->update([
                        'countBest'=> $ctbest,
                        'best'=>$best
                        ]);
                }else{
                    $goal->update([
                        'countBest'=> $ctbest,
                        ]); 
                }   
            }

            return 'them moi';
        }
    }
    public function delDate()
    {
        $goal_id = Input::get('goal_id');
        $dateIn = Input::get('date');
        $rs = Date::where('goal_id',$goal_id)->where('dateIn',$dateIn)->get()->first();
        if($rs != null){
            $rs->delete();
            return 'deleted';
        }else{
            return 'not deleted';
        }
    }
   public function listgoals()
   {    
        $user_id = Session::get('id');
        $goals = Goal::where('user_id',$user_id)->get();
       return view('partials.listToday',compact('goals'));
   }
   public function searchgoals()
   {    
        $key = Input::get('key');
        $user_id = Session::get('id');
        $goals = DB::table('goals')->where('title','like','%'.$key.'%')
                                   ->orwhere('description','like','%'.$key.'%')
                                   ->orwhere('dateStart','like','%'.$key.'%')->get();
       return view('partials.searchgoals',['goals'=>$goals,'key'=>$key]);
   }
    public function update($id, EditGoalFormRequest $request)
    {
        $inWeek=$request->input('2');
        for($i = '3';$i<'9';$i++){
            if($i == '8') {
                $name = 'CN';
            }else{
                $name = $i;
            }
            $inWeek = $inWeek.','.$request->input($name);
        }
        $goal = Goal::find($id);
        $goal->update([
            'title'=> $request->get('title'),
            'description'=>$request->get('description'),
            'dateStart'=> $request->get('dateStart'),
            'timeStart'=> $request->get('timeStart'),
            'repetition'=> $request->get('repetition'),
            'end' => $request->get('end'),
            'iterator'=> $request->get('iterator'),
            'reminder'=> $request->get('reminder'),
            'autoX'=> $request->get('autoX'),
            'dateEnd' => $request->get('dateEnd'),
            'every' => $request->get('every'),
            'inWeek' => $inWeek
        ]);

        function inGoal($date, $goal){
            $x = true;
            if($goal->end == 'Until a date'){
                if(strtotime($goal->dateStart) > strtotime($date) || strtotime($goal->dateEnd) < strtotime($date)) {
                    $x = false;
                }
            }else if($goal->end == 'Number of iteration'){
                if(strtotime($goal->dateStart) > strtotime($date) || $goal->iterator <= 0){
                    $x = false;
                }
            }else if($goal->end == 'Forever'){
                if(strtotime($goal->dateStart) > strtotime($date)){
                    $x = false;
                }
            }else{
                $x = true;
            }
            return $x;
        }
        function dateInWeek($date){
            $dateNumOfWeek = date('w',strtotime($date));
            $x = 'sai';
            if($dateNumOfWeek == '0'){
                $x = 'CN';
            }else{
                $x = $dateNumOfWeek+'1';
            }
            return $x;
        }

        function eventToday($date,$goal){
            $x =false;
            /*=========ARRAY DAY IN WEEK IN GOAL==========*/
            $arr = explode(',', $goal->inWeek);
            $dayFromStart = floor((strtotime($date) - strtotime($goal->dateStart))/(24*60*60));
            $monthFromStart = (date("Y",strtotime($date))-date("Y",strtotime($goal->dateStart)))*12+(date("n",strtotime($date))-date("n",strtotime($goal->dateStart)));
            $yearFromStart = date("Y",strtotime($date))-date("Y",strtotime($goal->dateStart));
            $weekFromStart = date("W",strtotime($date))-date("W",strtotime($goal->dateStart));
            if(($goal->repetition=='Daily') && inGoal($date,$goal) && ($dayFromStart%($goal->every)== 0)){
                $x =true;
            }
            else if(($goal->repetition=='Monthly')&& inGoal($date,$goal) && ($monthFromStart)%($goal->every)== 0 && (date("j",strtotime($date))==date("j",strtotime($goal->dateStart)))){
                $x =true;
            }
            else if(($goal->repetition=='Yearly') && inGoal($date,$goal) && ($yearFromStart)%($goal->every)== 0 && (date("j",strtotime($date))==date("j",strtotime($goal->dateStart))) && (date("n",strtotime($date))==date("n",strtotime($goal->dateStart)))){
                $x =true;
            }
            else if(($goal->repetition=='Weekly') &&  inGoal($date,$goal) && ($weekFromStart)%($goal->every)== 0 && in_array(dateInWeek($date), $arr)){
                $x =true;
            }
            return $x;
        }
        
        $dates = Date::where('goal_id',$id)->get();
        foreach ($dates as $date) {
            if(!eventToday($date->dateIn,$goal)){
                $date->delete();
            }
        }

        $user_id = Session::get('id');
        $goals = Goal::where('user_id',$user_id)->get();
        session::put('goals',$goals);
        return redirect()->route('goal.view', $id);
    }
    public function store(GoalFormRequest $request)
    {
        Session::forget('goals');
    	$title = $request->input('title');
    	$description = $request->input('description');
    	$dateStart = $request->input('dateStart');
    	$timeStart = $request->input('timeStart');
    	$repetition = $request->input('repetition');
        $end = $request->input('end');
    	$reminder = $request->input('reminder');
        $user_id = Session::get('id');

        $inWeek=$request->input('2');
        for($i = '3';$i<'9';$i++){
            if($i == '8') {
                $name = 'CN';
            }else{
                $name = $i;
            }
            $inWeek = $inWeek.','.$request->input($name);
        }

    	if ($request->input('autoX')=='1') {
    		$autoX = $request->input('autoX');	
		}else{
			$autoX = 0;
		}
        $every = $request->input('every');
		$end = $request->input('end');
		if($end == 'Until a date'){
		 	$dateEnd = $request->input('dateEnd');
            Goal::create([
            'title'=> $title,
            'description'=> $description,
            'dateStart'=> $dateStart,
            'timeStart'=> $timeStart,
            'repetition'=> $repetition,
            'end'=>$end,
            'reminder'=> $reminder,
            'autoX'=> $autoX,
            'dateEnd' => $dateEnd,
            'inWeek' => $inWeek,
            'user_id'  => $user_id,
            'every' => $every
            ]);
		}else if($end == 'Number of iteration'){
            $iterator = $request->input('iterator');
            Goal::create([
            'title'=> $title,
            'description'=> $description,
            'dateStart'=> $dateStart,
            'timeStart'=> $timeStart,
            'repetition'=> $repetition,
            'end'=>$end,
            'iterator'=> $iterator,
            'reminder'=> $reminder,
            'autoX'=> $autoX,
            'inWeek' => $inWeek,
            'user_id'  => $user_id,
            'every' => $every
            ]);
		    //$dateEnd = date("Y-m-d",strtotime("$dateStart + $numDates day"));
		}else{
            Goal::create([
            'title'=> $title,
            'description'=> $description,
            'dateStart'=> $dateStart,
            'timeStart'=> $timeStart,
            'repetition'=> $repetition,
            'end'=>$end,
            'reminder'=> $reminder,
            'autoX'=> $autoX,
            'inWeek' => $inWeek,
            'user_id'  => $user_id,
            'every' => $every
            ]);
        }
    	$goals = Goal::where('user_id',$user_id)->get();
        session::put('goals',$goals);
    	
    	return redirect()->route('page.main');
    }
    public function destroy($id)
    {
        $goal = Goal::find($id);
        $dates = Date::where('goal_id',$id)->get();
        foreach ($dates as $date ) {
            $date->delete();
        }
        $goal->delete();
        $user_id = Session::get('id');
        $goals = Goal::where('user_id',$user_id)->get();
        session::put('goals',$goals);
        return redirect()->route('page.main');
    }
}
