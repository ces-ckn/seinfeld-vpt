<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;
use Illuminate\Support\Facades\Mail as Mail;

class ContactsController extends Controller
{
    //
    public function postContact()
    {
        
        $email = Input::get('email');
        $title = Input::get('title');
        $message = Input::get('message');
        Contact::create([
        	'email' => $email,
        	'title' => $title,
        	'message' => $message
        ]);
        Mail::send('emails.contact', [], function($message) {
            $message->to(Input::get('email'))
                ->subject('Feedback')->from('vptteam123@gmail.com', "VPT");
        });

        return 'Thank you for your comment';
        
    }
}
