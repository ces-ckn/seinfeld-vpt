<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Session;
use Cookie;
use Flash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use App\Goal;
use App\Contact;

use App\Http\Requests\RegisterFormRequest;

class PagesController extends Controller
{
    public function index()
    {
    	return view('pages.index');
    }
    
    public function contact()
    {
        $contact = Contact::all();
    	return view('pages.contactmanagement',compact('contact'));
    }

    public function register()
    {
        
    	return view('pages.register');
    }
    public function store(RegisterFormRequest $request)
    {
        /*$name = Input::get('name');
        $email = Input::get('email');
        $password = Input::get('password');
        User::create([
            'name'=>$name,
            'email'=>$email,
            'password'=>$password
            ]);
        return redirect()->route('page.main');*/
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $user->save();

        Flash::message('Thanks for signing up! Please check your email.');
        return redirect()->route('page.main');
        
    }

    public function main()
    {   
        $id = Session::get('id');
        $paginateGoals = Goal::where('user_id',$id)->paginate(10);
        return view('pages.main',compact('paginateGoals'));
    }
    public function view()
    {
        return view('pages.view');
    }


    public function userInformation(){
        $user = User::find(Session::get('id'));
        return view('pages.userinformation', compact('user'));
    }
    
}
