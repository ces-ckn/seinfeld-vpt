<?php

namespace App\Http\Controllers;

use Input;
use Validator;
use Redirect;

use Storage;
use Illuminate\Http\Request;
use App\User;
use Session;
use App\Http\Requests;
use App\Http\Requests\LoginFormRequest;
use App\Http\Controllers\Controller;

class UserInformationController extends Controller
{
    //

    public function basicInf(){
        $id = Session::get('id');
        $user = User::find($id);
    	return view('partials/userinformation/basic-information', compact('user'));
    }
    public function accountInf(){
        $id = Session::get('id');
        $user = User::find($id);
    	return view('partials/userinformation/account-information', compact('user'));
    }
    public function editBasicInf(){
        $id = Session::get('id');
        $user = User::find($id);
    	return view('partials/userinformation/edit-basic-information', compact('user'));
    }
    public function editAccountInf(){
        $id = Session::get('id');
        $user = User::find($id);
    	return view('partials/userinformation/edit-account-information', compact('user'));
    }

    public function upload() {
        // getting User Information
        $user = User::find(Session::get('id'));       

        // getting all of the post data
        $file = array('image' => Input::file('image'));
        // setting up rules
        $rules = array('image' => 'required',); //mimes:jpeg,bmp,png and for max size max:10000
        // doing the validation, passing post data, rules and the messages
        $validator = Validator::make($file, $rules);
        if ($validator->fails()) {
        // send back to the page with the input data and errors
        return Redirect::to('upload')->withInput()->withErrors($validator);
        }
        else {
            // checking file is valid.
            if (Input::file('image')->isValid()) {
              // destination upload path
              $destinationPath = 'userImg'; 
              // getting image extension
              $extension = Input::file('image')->getClientOriginalExtension(); 
              // renameing image
              $fileName = 'user'.$user->id.'.'.$extension; 
              // uploading file to given path
              Input::file('image')->move($destinationPath, $fileName); 

              // sending the path of fileName to database
              $user->update([
                'avatar'  =>  '\userImg\\'.$fileName
              ]);
              session::put('avatar',$user->avatar);

              // sending back with message
              Session::flash('success', 'Upload successfully'); 
              return redirect()->route('page.userinformation');
            }
        }
    }

    public function updateBasicInf(Request $request){

      $user = User::find(Session::get('id'));
      
      $user->update([
        'name'  =>  $request->get('name'),
        'birthday'  =>  $request->get('birthday'),
        'gender'  =>  $request->get('gender'),
        'hometown'  =>  $request->get('hometown')
      ]);

      return redirect()->route('page.userinformation');
    }


    public function updateAccountInf(LoginFormRequest $request){

      $user = User::find(Session::get('id'));

      $user->update([
        'email' =>  $request->get('email'),
        'password'  =>  $request->get('password')
      ]);

      return redirect()->route('page.userinformation');
    }


}
