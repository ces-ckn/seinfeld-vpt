<?php

namespace App\Http\Controllers;
use Session;
use Cookie;
use App\Services;
use DB;
use Illuminate\Http\Request;
use App\User;
use App\Goal;

use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use Auth;
use Hash;
/*use Alert;*/
/*use Flash;*/
use Illuminate\Support\Facades\Mail as Mail;
use MailQueue;
use App\Http\Requests\RegisterFormRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\LoginFormRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class UsersController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    
    public function logout()
    {
        Session::flush();
        return redirect()->route('page.index');
    }
    public function postLogin(LoginFormRequest $request)
    {
        
       
        $user = new User;
        $email = $request->input('email');
        $password = $request->input('password');

        // set the remember me cookie if the user check the box
        $remember = (Input::has('remember')) ? true : false;

        $user = User::all();
        foreach ($user as $element ) {
            if($element->email==$email ){
                if($element->password==$password){
                    if($element->confirmed){

                        if ($element->status) {
                             session::put('id',$element->id);
                             session::put('name',$element->name);
                             session::put('email',$element->email);
                             session::put('password',$element->password);
                             session::put('role',$element->role);
                             session::put('status',$element->status);
                             $id = Session::get('id');

                             $user = User::find($id);
                             session::put('avatar',$user->avatar);

                             $goals = Goal::where('user_id',$id)->get();
                             session::put('goals',$goals);
                             session::put('user',$user);
                             return redirect()->route('page.main');
                        } else {
                            
                            Session::flash('notifyAccount','Your account was blocked by admin');
                            return redirect()->route('page.index');                        
                        }
                        
                    }else{
                        // message : You need verify your account at email.
                        session::flash('notifyAccount','You need to verify your account at email.');
                        return redirect()->route('page.index');

                    }
                }else{
                        session::flash('notifyAccount','Your password is incorrect');
                        return redirect()->route('page.index');
                }
            }
        }
        session::flash('notifyAccount','This account still not yet registered!');
        return redirect()->route('page.index');
        
    }

    public function register()
    {
        
    	return view('pages.register');
    }

    public function storeajax()
    {
        $name = Input::get('username');
        $email = Input::get('email');
        $password = Input::get('password');
       
       /* $password_confirmation = Input::get('password_confirmation');*/
        $confirmation_code = str_random(30);
        User::create([
            'name'  => $name,
            'email' => $email,
            'password'=> $password,
            'confirmation_code'=> $confirmation_code
        ]);
        $data['confirmation_code'] = $confirmation_code;

        Mail::send('emails.verify', $data, function($message) use ($confirmation_code){
            $message->to(Input::get('email'), Input::get('name'))
                ->subject('Verify your email address');
        });
        return 'success';
    }

    /*public function store(RegisterFormRequest $request)
    {
        $user = new User;
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = $request->input('password');
        $password_confirmation = $request->input('password_confirmation');
        $confirmation_code = str_random(30);
        $user->confirmation_code = $confirmation_code;
        $data['confirmation_code'] = $confirmation_code;
        
        $user->save();
        Mail::send('emails.verify', $data, function($message) use ($confirmation_code){
            $message->to(Input::get('email'), Input::get('name'))
                ->subject('Verify your email address');
        });
        Session::flash('notifyAccount', 'Thanks for signing up! Please check your email.'); 

        return redirect()->route('page.index');
        
    }*/

     public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->status = 1;
        $user->save();

        /*Flash::info('You have successfully verified your account.');*/
        Session::flash('notifyAccount', 'You have successfully verified your account..'); 


        return Redirect::route('page.index');
    }


    
    public function forgotPassword()
    {
        return view('pages.forgotpassword');
    }

    protected $mailer;
    function _construct(MailQueue $mailer){
        $this->mailer = $mailer;
    }

    public function resetPassword(ResetPasswordRequest $request)
    {   
        $user = new User;
        $email = $request->input('email');
        $user = User::all();
        foreach ($user as $element ) {
            if($element->email==$email){
                //send password to user's email
                $password = $element->password;
                $data['password']  = $element->password;
                $data['email']  = $element->email;

                Mail::send('emails.resetpw', $data, function($message) use ($password)
                {
                    $message->from('vptteam123@gmail.com', "VPT");
                    $message->subject("[ChainCalendar] Reset password");
                    $message->to(Input::get('email'));
                });
                // Your password was sent to your email

            }
        }
         Session::flash('notifyAccount', 'Your password was sent! Please check your email.');
        return redirect()->route('page.index');
    }

    public function management()
    {
        $num = 1;
        $users = DB::table('users')->get();
        return view('pages.usermanagement',['users'=>$users,'num'=>$num]);

    }

    // 
    public function buttonAdmin()
    {
        //check which submit was clicked on
        if(Input::get('lock')) {
            $this->lockUser(); //if login then use this method
        }elseif(Input::get('delete')) {
            $this->deleteUser(); //if register then use this method
        }elseif (Input::get('admin')){
            $this->makeAdmin();
           
        }

        return Redirect::back();
        
    }    
    // Delete any users who checked in manage users form
    public function deleteUser(){
       $usersChecked = Input::get('users');
       if($usersChecked==null) return Redirect::back();

       foreach ($usersChecked as $userChecked) {
            DB::table('users')->where('id', '=', $userChecked)->delete();
       }   
       
    }
    // Lock/Unlock any users who checked in manage users form
    public function lockUser(){
        $usersChecked = Input::get('users');
        if($usersChecked==null) return Redirect::back();

       foreach ($usersChecked as $userChecked) {
            $user = User::find($userChecked);
            if($user->status){
                $user->status = 0;
                //Session::flush();
                //this.Session::flush();
            }else{
                $user->status = 1;
            }
            $user->save();
       }  
       
    }

    public function makeAdmin()
   {    
        $usersChecked = Input::get('users');
        if($usersChecked==null) return Redirect::back();
       foreach ($usersChecked as $userChecked) {
            $user = User::find($userChecked);
            $user->role = 1;
            $user->status=1;
        
            $user->save();

        }
   }

   

}
