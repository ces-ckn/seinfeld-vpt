<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail as Mail;
use Session;
use App\Services;
use DB;
use Illuminate\Http\Request;
use App\User;
use App\Goal;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use Auth;
use Hash;
use Alert;
use MailQueue;
use App\Http\Requests\RegisterFormRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\LoginFormRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;


class Reminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This class reminders the user with our goals.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    

  
    
    
    public function handle()
    {

        function inGoal($date, $goal){
            $x = true;
            if($goal->end == 'Until a date'){
                if(strtotime($goal->dateStart) > strtotime($date) || strtotime($goal->dateEnd) < strtotime($date)) {
                    $x = false;
                }
            }else if($goal->end == 'Number of iteration'){
                if(strtotime($goal->dateStart) > strtotime($date) || $goal->iterator <= 0){
                    $x = false;
                }
            }else if($goal->end == 'Forever'){
                if(strtotime($goal->dateStart) > strtotime($date)){
                    $x = false;
                }
            }else{
                $x = true;
            }
            return $x;
        }


         function dateInWeek($date){
            $dateNumOfWeek = date('w',strtotime($date));
            $x = 'sai';
            if($dateNumOfWeek == '0'){
                $x = 'CN';
            }else{
                $x = $dateNumOfWeek+'1';
            }
            return $x;
        }


        function eventToday($date,$goal){
            $x =false;
            /*=========ARRAY DAY IN WEEK IN GOAL==========*/
            $arr = explode(',', $goal->inWeek);
            $dayFromStart = floor((strtotime($date) - strtotime($goal->dateStart))/(24*60*60));
            $monthFromStart = (date("Y",strtotime($date))-date("Y",strtotime($goal->dateStart)))*12+(date("n",strtotime($date))-date("n",strtotime($goal->dateStart)));
            $yearFromStart = date("Y",strtotime($date))-date("Y",strtotime($goal->dateStart));
            $weekFromStart = date("W",strtotime($date))-date("W",strtotime($goal->dateStart));
            if(($goal->repetition=='Daily') && inGoal($date,$goal) && ($dayFromStart%($goal->every)== 0)){
                $x =true;
            }
            if(($goal->repetition=='Monthly')&& inGoal($date,$goal) && ($monthFromStart)%($goal->every)== 0 && (date("j",strtotime($date))==date("j",strtotime($goal->dateStart)))){
                $x =true;
            }
            if(($goal->repetition=='Yearly') && inGoal($date,$goal) && ($yearFromStart)%($goal->every)== 0 && (date("j",strtotime($date))==date("j",strtotime($goal->dateStart))) && (date("n",strtotime($date))==date("n",strtotime($goal->dateStart)))){
                $x =true;
            }
            if(($goal->repetition=='Weekly') &&  inGoal($date,$goal) && ($weekFromStart)%($goal->every)== 0 && in_array(dateInWeek($date), $arr)){
                $x =true;
            }
            return $x;
        }

        function exactTime($goal){
                $x =false;
                date_default_timezone_set("Asia/Ho_Chi_Minh");
                $currentTime = strtotime(date("Y-m-d H:i:s"));
                $currentDate = date("Y-m-d");
                $timeStart1 = $goal->timeStart;
                $timeStart2 = "$currentDate $timeStart1";
                $timeStart3 = strtotime($timeStart2);
                
                $count = round(($timeStart3 - $currentTime) / 60);
                switch ($goal->reminder) {
                    case '15 minutes':
                        if($count>=13&&$count<=17) $x = true;
                        break;
                    case '30 minutes':
                        if($count>=28&&$count<=32) $x = true;
                        break;
                    case '1 hour':
                        if($count>=58&&$count<=62) $x = true;
                        break;
                    
                    default:
                        if($count>=178&&$count<=182) $x = true;
                        break;
                }
                    return $x;
               /* return false;*/
        }
/*
        $to_time = strtotime("2008-12-13 10:42:00");
        $from_time = strtotime("20to_time08-12-13 10:21:00");
        echo round(abs($dateCurrent - $datetimeStart) / 60,2). " minute";*/

        $date = date("Y-m-d");
        $goals = Goal::all();
        foreach($goals as $goal){
            if(eventToday($date,$goal)){
                if(exactTime($goal)){
                    $user = User::where('id',$goal->user_id)->get()->first();
                    $data['goal'] = $goal->title;
                    $data['email'] = $user->email;
                    $data['name'] = $user->name;
                    Mail::send('emails.reminder',$data, function($message) use ($data)
                    {
                    $message->from('vptteam123@gmail.com', "VPT");
                    $message->subject("[ChainCalendar] Reminder ");
                    $message->to($data['email']);
                    });
                }
            }
             
        }
       
    }

}
