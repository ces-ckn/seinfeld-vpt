<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Goal;
use App\Date;
use App\User;
class AutoMark extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auto:mark';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will add fail date auto if no mark';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    
    
    
    public function handle()
    {
        function inGoal($date, $goal){
            $x = true;
            if($goal->end == 'Until a date'){
                if(strtotime($goal->dateStart) > strtotime($date) || strtotime($goal->dateEnd) < strtotime($date)) {
                    $x = false;
                }
            }else if($goal->end == 'Number of iteration'){
                if(strtotime($goal->dateStart) > strtotime($date) || $goal->iterator <= 0){
                    $x = false;
                }
            }else if($goal->end == 'Forever'){
                if(strtotime($goal->dateStart) > strtotime($date)){
                    $x = false;
                }
            }else{
                $x = true;
            }
            return $x;
        }
        function expiredGoals($date, $goal){
            $x = false;
            if($goal->end == 'Until a date'){
                if(strtotime($goal->dateEnd) < strtotime($date)) {
                    $x = true;
                }
            }else if($goal->end == 'Number of iteration'){
                if($goal->iterator <= 0){
                    $x = true;
                }
            }else{
                $x = false;
            }
            return $x;
        }
        function dateInWeek($date){
            $dateNumOfWeek = date('w',strtotime($date));
            $x = 'sai';
            if($dateNumOfWeek == '0'){
                $x = 'CN';
            }else{
                $x = $dateNumOfWeek+'1';
            }
            return $x;
        }
        function eventToday($date,$goal){
            $x =false;
            /*=========ARRAY DAY IN WEEK IN GOAL==========*/
            $arr = explode(',', $goal->inWeek);
            $dayFromStart = floor((strtotime($date) - strtotime($goal->dateStart))/(24*60*60));
            $monthFromStart = (date("Y",strtotime($date))-date("Y",strtotime($goal->dateStart)))*12+(date("n",strtotime($date))-date("n",strtotime($goal->dateStart)));
            $yearFromStart = date("Y",strtotime($date))-date("Y",strtotime($goal->dateStart));
            $weekFromStart = date("W",strtotime($date))-date("W",strtotime($goal->dateStart));
            if(($goal->repetition=='Daily') && inGoal($date,$goal) && ($dayFromStart%($goal->every)== 0)){
                $x =true;
            }
            if(($goal->repetition=='Monthly')&& inGoal($date,$goal) && ($monthFromStart)%($goal->every)== 0 && (date("j",strtotime($date))==date("j",strtotime($goal->dateStart)))){
                $x =true;
            }
            if(($goal->repetition=='Yearly') && inGoal($date,$goal) && ($yearFromStart)%($goal->every)== 0 && (date("j",strtotime($date))==date("j",strtotime($goal->dateStart))) && (date("n",strtotime($date))==date("n",strtotime($goal->dateStart)))){
                $x =true;
            }
            if(($goal->repetition=='Weekly') &&  inGoal($date,$goal) && ($weekFromStart)%($goal->every)== 0 && in_array(dateInWeek($date), $arr)){
                $x =true;
            }
            return $x;
        }
        $dayCurrent = date("d");
        $monthCurrent = date("m");
        $yearCurrent = date("Y");
        $date = "$yearCurrent-$monthCurrent-$dayCurrent";
        $goals = Goal::all();
        foreach($goals as $goal){
            if(eventToday($date,$goal)){
                $goal_id = $goal->id;
                $dateIn = $date;
                if($goal->autoX == 0){
                    $result = 2;
                }else{
                    $result = 0;      
                }
                $rs = Date::where('goal_id',$goal_id)->where('dateIn',$dateIn)->get()->first();
                if($rs == null){
                    if($goal->end == "Number of iteration"){
                    $itr = $goal->iterator-1;
                    $goal->update([
                        'iterator'=> $itr
                        ]);
                    }
                    Date::create([
                        'goal_id' =>  $goal_id,
                        'dateIn'  =>  $dateIn,
                        'result'  =>  $result,
                        ]);
                    $ctBest = 0;
                    $goal->update([
                        'countBest'=>$ctBest
                    ]);
                }
            }
            if(expiredGoals($date, $goal)){
                $dates = Date::where('goal_id',$goal->id)->get();
                foreach ($dates as $date ) {
                    $date->delete();
                }
                $goal->delete();
            }
        }
    }
}
