<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $table = 'dates';
    protected $fillable = [
    	'goal_id',
    	'dateIn',
        'result'
    ];
}
