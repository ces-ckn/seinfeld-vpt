<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('goals', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')->on('users');
            $table->string('title');
            $table->string('description');
            $table->date('dateStart');
            $table->time('timeStart');
            $table->string('repetition');
            $table->string('end');
            $table->integer('iterator');
            $table->date('dateEnd');
            $table->string('inWeek');
            $table->string('reminder');
            $table->boolean('autoX');
            $table->integer('every');
            $table->integer('best');
            $table->integer('countBest');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('goals');
    }
}
